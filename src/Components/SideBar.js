import React, { useState, useEffect, useContext } from "react";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import BallotIcon from "@material-ui/icons/Ballot";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import AssessmentIcon from "@material-ui/icons/Assessment";
import { Link } from "react-router-dom";

import { useTheme } from "@material-ui/core/styles";
import Hidden from "@material-ui/core/Hidden";
import CssBaseline from "@material-ui/core/CssBaseline";

import { ContextHead } from "../context/ContextHead";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
  },
  // drawer: {

  //     width: drawerWidth,
  //     flexShrink: 0,

  // },
  drawerPaper: {
    width: drawerWidth,

    opacity: 1,
  },
  // drawerContainer: {
  //   overflow: "auto",
  // },
  root: {
    display: "flex",
  },
  typo: {
    fontFamily: "Raleway",
    fontWeight: 700,
  },
  listtext: {
    color: "black",
  },
}));

export default function SelectPage(props) {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const theme = useTheme();

  const { mobileOpen, userPermission } = React.useContext(ContextHead);
  const [mobileOpen1, setMobileOpen1] = mobileOpen;
  const [userPermission1, setuserPermission1] = userPermission;

  const handleChange = (e, newValue) => {
    props.setValue(newValue);
  };

  const ListOption = [
    {
      name: "Get Started",
      icon: <BallotIcon />,
      link: "/select",
      index: 0,
      role: "emp",
    },
    {
      name: "Progress",
      icon: <CreditCardIcon />,
      link: "/progress",
      index: 1,
      role: "emp",
    },
    {
      name: "Employee Details",
      icon: <SupervisorAccountIcon />,
      link: "/employee",
      index: 2,
      role: "emp",
    },
    {
      name: "Report",
      icon: <AssessmentIcon />,
      link: "/report",
      index: 3,
      role: "emp",
    },
    {
      name: "Requests",
      icon: <BallotIcon />,
      link: "/techlead/requests",
      index: 4,
      role: "tech",
    },

    {
      name: "Reports",
      icon: <CreditCardIcon />,
      link: "/techlead/reports",
      index: 5,
      role: "tech",
    },

    {
      name: "Requests",
      icon: <BallotIcon />,
      link: "/accountant/requests",
      index: 6,
      role: "acc",
    },

    {
      name: "Reports",
      icon: <CreditCardIcon />,
      link: "/accountant/reports",
      index: 7,
      role: "acc",
    },

    {
      name: "Admin Pannel",
      icon: <SupervisorAccountIcon />,
      link: "/admin",
      index: 8,
      role: "admin",
    },
  ];

  useEffect(() => {
    if (window.location.pathname === "/" && value !== 0) {
      setValue(0);
    } else if (window.location.pathname === "/progress" && value !== 1) {
      setValue(1);
    } else if (window.location.pathname === "/employee" && value !== 2) {
      setValue(2);
    } else if (window.location.pathname === "/report" && value !== 3) {
      setValue(3);
    } else if (window.location.pathname === "/requests" && value !== 4) {
      setValue(4);
    } else if (window.location.pathname === "/reports" && value !== 5) {
      setValue(5);
    } else if (window.location.pathname === "/arequest" && value !== 6) {
      setValue(6);
    } else if (window.location.pathname === "/areport" && value !== 7) {
      setValue(7);
    } else if (window.location.pathname === "/admin" && value !== 8) {
      setValue(8);
    }
  }, [value]);

  const drawers = (
    <React.Fragment>
      <Toolbar />
      <div className={classes.drawerContainer}>
        <List value={value} onChange={handleChange}>
          {ListOption.map(({ name, icon, link, index, role }, i) => {
            if (role === userPermission1)
                      //  console.log(role)
              return (
                //   <ShowPermission permissions={role} user={currentuserRole}>
                <ListItem
                  button
                  component={Link}
                  to={link}
                  onClick={() => {
                    setValue(index);
                    setMobileOpen1(false);
                  }}
                  selected={value === index}
                  style={value === index ? { backgroundColor: "#EC2329" } : {}}
                >
                  <ListItemIcon
                    style={value === index ? { color: "white" } : {}}
                  >
                    {icon}
                  </ListItemIcon>

                  <ListItemText
                    className={classes.listtext}
                    style={value === index ? { color: "white" } : {}}
                    primary={name}
                  />
                </ListItem>
              );
            //       </ShowPermission>
          })}
        </List>
      </div>
    </React.Fragment>
  );

  return (
    <div className={classes.root}>
      <CssBaseline />

      <nav className={classes.drawer} aria-label="mailbox folders">
        {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
        <Hidden smUp implementation="css">
          <Drawer
            variant="temporary"
            anchor={theme.direction === "rtl" ? "right" : "left"}
            open={mobileOpen1}
            onClose={() => setMobileOpen1(false)}
            onOpen={() => setMobileOpen1(true)}
            classes={{
              paper: classes.drawerPaper,
            }}
          >
            {drawers}
          </Drawer>
        </Hidden>
        <Hidden smDown implementation="css">
          <Drawer
            classes={{
              paper: classes.drawerPaper,
            }}
            variant="permanent"
            open
          >
            {drawers}
          </Drawer>
        </Hidden>
      </nav>
    </div>
  );
}
