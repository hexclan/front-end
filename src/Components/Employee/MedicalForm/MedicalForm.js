import { FormControl, FormControlLabel,FormLabel,
    Select, Grid, Radio, RadioGroup, TextField, Button, Paper } from '@material-ui/core'

import {React,useState,useContext,useEffect} from 'react'
import { Link, useHistory } from "react-router-dom";


import { makeStyles } from "@material-ui/core";

import MedicalTable from './MedicalFormTable'
import AddIcon from '@material-ui/icons/Add';
import Popup from './PopupM'
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import {ContextState} from '../../../context/contexStates'
import { addFormM,getTech, getDependent,addEmail } from '../../../services/MedicalForm';
import {GlobalContext} from "../../../context/GlobalContext"
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';




function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  

const initialFValues = {
 
    reporttitle: '',

    paymentmethode:'bank',
    doctorName:'',
    date: new Date(),
    techleadname:'',
    natureofinjury:'',
    personName:[]
 
}



const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiFormControl-root': {
            width: '80%',
            margin: theme.spacing(2)
        },
        
        
       
    },
    Button:{
        flex:"right",
        marginTop:"3rem",
        color:"white"
    },
    newbutton:{
         marginLeft:"0rem",
         marginBottom:"1rem"
        
    },
Paper1: {
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        
    },
Snackbar:{
    top :theme.spacing(9)
}

}))


export default function MedicalForm() {

  

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('reporttitle' in fieldValues)
            temp.reporttitle = fieldValues.reporttitle ? "" : "This field is required."
            if ('doctorName' in fieldValues)
            temp.doctorName = fieldValues.doctorName? "" : "This field is required."
            if ('techleadid' in fieldValues)
            temp.techleadid = (/^[0-9]*$/).test(fieldValues.techleadid)? "" : "This field is required."
            if ('natureofinjury' in fieldValues)
            temp.natureofinjury = fieldValues.natureofinjury ? "" : "This field is required."
       
        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }



 const classes = useStyles();

const [values,setValues]  =useState(initialFValues);
const [openPopup,setOpenPopup] =useState(false);

const[errors,setErrors]=useState({})

const { records } = useContext(ContextState);
const [records1,setRecords1]  = records
const [openNoti,setOpenNoti] = useState(false)
const history = useHistory();
const [personName, setPersonName] = useState([]);

const [techName, setTechName] = useState([]);



useEffect(async () => {
     
      getTech()
    .then((res) => {
      setTechName(res);
     
    })
    .catch((err) => {
      console.log(err);
    });

    
    getDependent(userDetails.employee.employeeId)
        .then((res) => {
          setPersonName(res);
      console.log(personName[0].dependentId)
          
         
        })
        .catch((err) => {
          console.log(err);
        });
    
    } ,[])





   
 

// const handleChange = (event) => {
//   setPersonName(event.target.value);
// };


const userDetails=JSON.parse(localStorage.getItem('user'))

const handleInputChange = e => {
    const { name, value } = e.target


    

    setValues({
        ...values,
        [name]: value
    })
   
}


// const dependent ={
//          dependentId: 1
// }

// const allDepend

const handleSubmit=e=>{
  
 
     e.preventDefault()
     if(validate()){
      
         const object1 = {

  
                submittedDate:values.date,
                amount:values.amount,
                techleadId :  values.techleadname,
                formDescription:values.reporttitle,
                doctorName : values.doctorName,
                diagnonsis:values.natureofinjury,
                progressStatus:"pending",
               paymentMethod:values.paymentmethode,
                receipt : records1,
                
                employee:{
                    employeeId: userDetails.employee.employeeId,
                    
               },

              //  dependent:dependent
              
              
              
          }
          console.log(records1)
                           
          const object2 = {
            receiverEmail: newValue,
    
            subject: "You got a medical claim request",
    
            text: "checkyour reimburser for a claim request",
          };
            

              
        addEmail(object2)
        .then((res) => {
          
        })
        .catch((err) => {
          console.log(err);
        });

      addFormM(object1)
        .then((res) => {
          history.push("/medical");
        })
        .catch((err) => {
          console.log(err);
        });


        resetForm()

     }
    
   
}

const techEmail = techName.map((name) => {
   
  if(values.techleadname==name.employeeId)
   
     return   name.email
    

});


const newValue = techEmail.find((email)=> email != undefined )


const resetForm = () => {
    setValues(initialFValues);
    setErrors({})
    setRecords1([]);
}


const handleClick = () => {
    if(validate())
    {setOpenNoti(true);}
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenNoti(false);
  };




    return (
        <div>
             <form className={classes.root}  onSubmit={handleSubmit}>
            <Grid container>
                <Grid item xs={12}  sm ={6}>
                    
                <TextField
                       variant="outlined"
                        name="reporttitle"
                        label="Report title"
                        value={values.reporttitle}
                        onChange={handleInputChange}
                        error={errors.reporttitle}
                
                       
                    />




<FormControl >
        <InputLabel id="demo-mutiple-name-label">Claim For</InputLabel>
        <Select
           multiple
           value={values.personName}
           onChange={handleInputChange}
          name="personName"
        >
          {personName.map((name) => (
            <MenuItem key={name.dependentId} value={name.dependentId} >
              {name.dependentName}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      
                    <FormControl>
                        <FormLabel>
                         Payment method
                        </FormLabel>
                        <RadioGroup
                        name="paymentmethode"
                        value={values.paymentmethode}
                        onChange={handleInputChange}>
                            <FormControlLabel value ="  bank" control={<Radio/>} label="Bank"/>
                            <FormControlLabel value ="cash" control={<Radio/>} label="Cash"/>
                  
                        
                        </RadioGroup>
                    </FormControl>
           </Grid>

           <Grid item xs={12} sm={6} >

           <TextField
                variant="outlined"
                        name="doctorName"
                        label="Doctor name"
                        value={values.doctorName}
                        onChange={handleInputChange}
                        error={errors.doctorName}
                    />


         

<FormControl className={classes.formControl}>
        <InputLabel >Techlead name</InputLabel>
        <Select
               name="techleadname"
          value={values.techleadname}
          onChange={handleInputChange}
        >
      
   
          {techName.map((name) => {
                        return (
                          <MenuItem value={name.employeeId}>
                            {name.firstName}
                          </MenuItem>
                        );
                      })}
              
              
              
          
          
        </Select>
      </FormControl>

         
                  <TextField
  id="outlined-multiline-static"
  label="Diagnosis or nature of illness"
  multiline
  rows={3}
  variant="outlined"
  name="natureofinjury"
  error={errors.natureofinjury}
  value={values.natureofinjury}
  onChange={handleInputChange}

/>
<TextField
        id="date"
        label="Date"
        type="date"
        defaultValue="2017-05-24"
        name="date"
        onChange={handleInputChange}
      
      />

            
         
           </Grid>

          </Grid>


          <Paper   className={classes.Paper1}>
            <Button variant="outlined" startIcon= {<AddIcon/>}
            className={classes.newbutton}
            onClick={()=> setOpenPopup(true)}
            > Add New </Button>
          
          <MedicalTable   />
               

    
         
      </Paper>
      <Button 
      type="submit"
      
      variant="contained" color="primary" className={classes.Button}
      onClick={handleClick}>
        Submit
      </Button>
      <Snackbar 
        className={classes.Snackbar}
         anchorOrigin={{vertical:"top",horizontal:"right"}}
      open={openNoti} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="success">
          Form submitted succesfully
         </Alert>
      </Snackbar>
      <Button
      type="reset"
      onClick={resetForm}
      variant="contained" color="secondary" className={classes.Button}>
        Reset
      </Button>
      
        </form>

        <Popup
        openPopup = {openPopup}
        setOpenPopup ={setOpenPopup}
        >
                  
        </Popup>
      
        </div>
    )}
