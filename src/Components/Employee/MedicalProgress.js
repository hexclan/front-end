import React ,{useContext}from "react";
import {MedicalContext} from "../../context/ContextMedical"
import styled from "styled-components";
export default function Expense() {


    const [Medical,SetMedical]  = useContext(MedicalContext)

  return (
    <ExpenseWrap>
      <div className="container table-responsive etable">
      <h4>Medical Progress </h4>
      <br/>
        <table class="table table-hover">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Report Title</th>
              <th scope="col">Date</th>
              <th scope="col">Techlead</th>
              <th scope="col">Status</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
          
             
              
                  <>
                  
                     {Medical.map((medical) => {
                          return (
                      <tr>
                        <th scope="row">1</th>
                        <td>{medical.report}</td>
                        <td>{medical.date}</td>
                        <td>{medical.techlead}</td>
                        <td>{medical.status.message}</td>
                        <td>
                        {(() => {
                            switch (medical.status.level) {
                              case 1:
                                return <StatusIndicator color="#F17E7E" />;
                              case 2:
                                return <StatusIndicator color="#FFD056" />;
                              case 3:
                                return <StatusIndicator color="#75C282" />;
                              default:
                                return <StatusIndicator color="#AAA5A5" />;
                            }
                          })()}
                          
                        </td>
                      </tr>
                       )  })}
                  </>
                
              
         
          </tbody>
        </table>
      </div>
    </ExpenseWrap>
  );
}
const StatusIndicator = styled.div`
  width: 15px;
  height: 15px;
  border-radius: 10px;
  background-color: ${(props) => props.color};
  /* position: absolute; */
`;

const ExpenseWrap = styled.div`
margin-left: 0px;


@media (min-width: 992px) {
  margin-left: 250px;
}
  thead {
    /* position: absolute; */
  }
  .etable {
    /* padding-left: 5rem; */
  }
 
`;
