import { React, useState } from "react";
import { withStyles, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { TablePagination, TableSortLabel, TextField } from "@material-ui/core";
import { SearchRounded } from "@material-ui/icons";
import InputAdornment from "@material-ui/core/InputAdornment";

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#EC2329",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

function createData(reporttitle, expensetype, date, amount, status) {
  return { reporttitle, expensetype, date, amount, status };
}

const rows = [
  createData("Fever", "medical", "2021-05-12", "3000", "Approved"),
  createData("fracture", "expense", "2021-04-12", "43", "Approved"),
  createData("vomit", "medical", "2023-04-12", "432", "Approved"),
  createData("tavel", "expense", "2021-04-12", "3545", "rejected"),
  createData("Food", "expense", "2020-04-12", "3300", "Approved"),
  createData("taxi", "expense", "2020-04-12"," 500", "Approved"),
  createData("corona", "medical", "2020-08-12", "4200", "rejected"),
];

const headCells = [
  { id: "reporttitle", lable: "Report title" },
  { id: "expensetype", lable: "Expense type" },
  { id: "date", lable: "Submitted date" },
  { id: "amount", lable: "Amount" },
  { id: "status", lable: "status" },
];

const useStyles = makeStyles({
  table: {
    minWidth: 700,
    width:"95%"
  },
});

export default function Reports() {
  const pages = [5, 10, 15];
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(pages[page]);
  const [order, setOrder] = useState();
  const [orderby, setOrderBy] = useState();
  const [filterFn, setFilterFn] = useState({
    fn: (items) => {
      return items;
    },
  });

  const classes = useStyles();

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  function stableSort(array, comparator) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  function getComparator(order, orderby) {
    return order === "desc"
      ? (a, b) => descendingComparator(a, b, orderby)
      : (a, b) => -descendingComparator(a, b, orderby);
  }

  function descendingComparator(a, b, orderby) {
    if (b[orderby] < a[orderby]) {
      return -1;
    }
    if (b[orderby] > a[orderby]) {
      return 1;
    }
    return 0;
  }

  const recordsAfterPagingAndSorting = () => {
    return stableSort(filterFn.fn(rows), getComparator(order, orderby)).slice(
      page * rowsPerPage,
      (page + 1) * rowsPerPage
    );
  };

  const handleSortRequest = (cellid) => {
    const isAsc = orderby === cellid && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(cellid);
  };

  const handleSearch = (e) => {
    let target = e.target;
    setFilterFn({
      fn: (items) => {
        if (target.value == "") return items;
        else
          return items.filter((x) =>
            x.reporttitle.toLowerCase().includes(target.value)||
            x.expensetype.toLowerCase().includes(target.value)||
            x.date.toLowerCase().includes(target.value)||
            x.amount.toLowerCase().includes(target.value)||
            x.status.toLowerCase().includes(target.value)
           
          );
      },
    });
  };


  return (
    <div>
      <TextField
        label="Search"
        onChange={handleSearch}
        variant="outlined"
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchRounded />
            </InputAdornment>
          ),
        }}
        style={{paddingBottom:"20px"}}
      />
    

      <TableContainer>
        <Table className={classes.table}  aria-label="customized table">
          <TableHead>
            <TableRow>
              {headCells.map((headcell) => (
                <StyledTableCell
                  align="left"
                  key={headcell.id}
                  sortDirection={orderby === headcell.id ? order : false}
                >
                  <TableSortLabel
                    active={orderby === headcell.id}
                    direction={orderby === headcell.id ? order : "asc"}
                    onClick={() => {
                      handleSortRequest(headcell.id);
                    }}
                  >
                    {headcell.lable}
                  </TableSortLabel>
                </StyledTableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {recordsAfterPagingAndSorting().map((row) => (
              <StyledTableRow key={row.reporttitle}>
                <StyledTableCell component="th" scope="row">
                  {row.reporttitle}
                </StyledTableCell>
                <StyledTableCell align="left">
                  {row.expensetype}
                </StyledTableCell>
                <StyledTableCell align="left">{row.date}</StyledTableCell>
                <StyledTableCell align="left">{row.amount}</StyledTableCell>
                <StyledTableCell align="left">{row.status}</StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>
        </Table>
        <TablePagination
        component="div"
        page={page}
        rowsPerPageOptions={pages}
        rowsPerPage={rowsPerPage}
        count={rows.length}
        onChangePage={handleChangePage}
        onChangeRowsPerPage={handleChangeRowsPerPage}
      />
      </TableContainer>
     
    </div>
  );
}
