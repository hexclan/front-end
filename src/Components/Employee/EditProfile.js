import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { AdminContext } from "../../context/adminContext";
import { Formik, Field, Form, ErrorMessage } from "formik";
import Validation from "./UserValidation";
import { editProfile, updateEmployee } from "../../services/user";
import { getUsers } from "../../services/user";
import { updateProfile } from "../../services/profilePicture";
export default function EditProfile(props) {
  const history = useHistory();


  
const userDetails=JSON.parse(localStorage.getItem('user'))
 
  const {
    firstName,
    lastName,
    phoneNo,
  
    address,
  } = userDetails;

  
  const initialValues = {
     firstName:userDetails.employee.firstName,
    lastName:userDetails.employee.lastName,
    phoneNo:userDetails.employee.phoneNo,

    address:userDetails.employee.address,
  };

  const onSubmit = (fields) => {};

  const handleSubmitting = (val) => {
    const {
      firstName,
    lastName,
      phoneNo,
   
      address,
    } = val;

    

    const editedProfile = {
        employeeId:userDetails.employee.employeeId,
       firstName,
    lastName,
      address,
  
      phoneNo,
    };

    editProfile(editedProfile)
      .then((res) => {
        
          localStorage.setItem('user',JSON.stringify(res))
            history.push("/employee");
          })
          .catch((err) => {
            console.log(err);
          });
      }

  return (
    <div className="container">
      <div className="row">
        <div className="card col-md-10 offset-md-2 offset-md-2">
          <h5 className="text-center">Edit User</h5>
          <div className="card-body">
            <Formik
              initialValues={initialValues}
              validationSchema={Validation}
              onSubmit={onSubmit}
            >
              {({ values }) => (
                <Form style={{ display: "flex", flexDirection: "column" }}>
                  <div className="col-6">
                    <label className="form-label">First Name</label>
                    <Field
                      name="firstName"
                      type="text"
                      placeholder="First Name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="firstName"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <div className="col-6">
                    <label className="form-label">Last Name</label>
                    <Field
                      name="lastName"
                      type="text"
                      placeholder="Last Name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="lastName"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  
                  
                  <div className="col-6">
                    <label className="form-label">Phone Number</label>
                    <Field
                      name="phoneNo"
                      type="text"
                      placeholder="phone number"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="phoneNo"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <div className="col-6">
                    <label className="form-label">Address</label>
                    <Field
                      name="address"
                      type="text"
                      placeholder="address"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="address"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
               {   /*<div className="col-6">
                    <label className="form-label">Designation</label>
                    <Field
                      name="designation"
                      type="text"
                      placeholder="designation"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="address"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>*/}
                  <div className="col-12">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => handleSubmitting(values)}
                    >
                      Update
                    </button>
                    <Link to={"/employee"}>
                      <button className="btn btn-danger">Cancel</button>{" "}
                    </Link>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
