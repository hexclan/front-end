import {
  FormControl,
  FormControlLabel,
  FormLabel,
  Select,
  Grid,
  Radio,
  RadioGroup,
  TextField,
  Button,
  Paper,
} from "@material-ui/core";

import { React, useState, useContext, useEffect } from "react";

import { makeStyles } from "@material-ui/core";

import ExpenseTable from "./ExpenseFormTable";
import AddIcon from "@material-ui/icons/Add";
import Popup from "./PopupE";
import { ContextState } from "../../../context/contexStates";
import { addFormE, addEmail } from "../../../services/ExpenseForm";
import { getTech } from "../../../services/MedicalForm";
import { Link, useHistory } from "react-router-dom";
import MuiAlert from "@material-ui/lab/Alert";
import Snackbar from "@material-ui/core/Snackbar";

import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";

const initialFValues = {
  reporttitle: "",

  paymentmethode: "",
  project: "",
  date: new Date(),
  techleadname: "",
};

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiFormControl-root": {
      width: "80%",
      margin: theme.spacing(2),
    },
  },
  Button: {
    flex: "right",
    marginTop: "3rem",
    color: "white",
  },
  newbutton: {
    marginLeft: "0rem",
    marginBottom: "1rem",
  },
  Paper1: {
    margin: theme.spacing(1),
    padding: theme.spacing(1),
  },
  Snackbar: {
    top: theme.spacing(9),
  },
}));

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function MedicalForm() {
  const validate = (fieldValues = values) => {
    let temp = { ...errors };
    if ("reporttitle" in fieldValues)
      temp.reporttitle = fieldValues.reporttitle
        ? ""
        : "This field is required.";
    if ("project" in fieldValues)
      temp.project = fieldValues.project ? "" : "This field is required.";
    
    setErrors({
      ...temp,
    });

    if (fieldValues == values) return Object.values(temp).every((x) => x == "");
  };

  const classes = useStyles();

  const [values, setValues] = useState(initialFValues);
  const [openPopup, setOpenPopup] = useState(false);

  const [errors, setErrors] = useState({});
  const [openNoti, setOpenNoti] = useState(false);


  const { ERecords,url } = useContext(ContextState);
  const [ERecords1, setERecords1] = ERecords;
  

 


  const history = useHistory();

  const userDetails=JSON.parse(localStorage.getItem('user'))



  const [techName, setTechName] = useState([]);

  useEffect(async () => {
    getTech()
      .then((res) => {
        setTechName(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setValues({
      ...values,
      [name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate()) {
      const object1 = {
        formDescription: values.reporttitle,
        paymentmethode: values.paymentmethode,
        project: values.project,
        submittedDate: values.date,
        techleadId: values.techleadname,
        progressStatus: "pending",
        image: ERecords1,
        amount: "3000",
        paymentMethod: values.paymentmethode,
        employee: {
          employeeId: userDetails.employee.employeeId,
        },
       

      };
     

      const object2 = {
        receiverEmail: newValue,

        subject: "You got a expense claim request",

        text: "checkyour reimburser for a claim request",
      };

      addEmail(object2)
        .then((res) => {
          
        })
        .catch((err) => {
          console.log(err);
        });

      addFormE(object1)
        .then((res) => {
          history.push("/expense");
        })
        .catch((err) => {
          console.log(err);
        });

      resetForm();
    }
  };

  const techEmail = techName.map((name) => {
   
       if(values.techleadname==name.employeeId)
        
          return   name.email
         
   
  });
  console.log(techEmail);

  const newValue = techEmail.find((email)=> email != undefined )
  console.log(newValue)


  // const numbers = ["", "","bye" ,"" ];
  // const newValue = numbers.find((nu)=> nu != "" );
  // console.log(newValue);

  // function techEmail (id){
  //   if(values.techleadname==65){
  //     return values.email
  //   }
  // }
  // console.log 




  const handleClick = () => {
    if (validate()) {
      setOpenNoti(true);
    }
  };

  const handleClose = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenNoti(false);
  };

  const resetForm = () => {
    setValues(initialFValues);
    setErrors({});
    setERecords1([]);
  };

  return (
    <div>
      <form className={classes.root} onSubmit={handleSubmit}>
        <Grid container>
          <Grid item xs={12} sm={6}>
            <TextField
              variant="outlined"
              name="reporttitle"
              label="Report title"
              value={values.reporttitle}
              onChange={handleInputChange}
              error ={errors.reporttitle}
              // error = {null}
              // {...(errors & { errors:true,helperText:errors})}


              error={errors.reporttitle}
            />

            <FormControl>
              <FormLabel>Payment method</FormLabel>
              <RadioGroup
                name="paymentmethode"
                value={values.paymentmethode}
                onChange={handleInputChange}
              >
                <FormControlLabel
                  value="bank"
                  control={<Radio />}
                  label="Bank"
                />
                <FormControlLabel
                  value="cash"
                  control={<Radio />}
                  label="Cash"
                />
              </RadioGroup>
            </FormControl>
          </Grid>

          <Grid item xs={12} sm={6}>
            <TextField
              variant="outlined"
              name="project"
              label="Project"
              value={values.project}
              onChange={handleInputChange}
              error={errors.project}
            />

            {/* 
           <TextField
                       variant="outlined"
                        name="techleadid"
                        label="Techlead name"
                        value={values.techleadid}
                        onChange={handleInputChange}
                        error={errors.techleadid}
                    /> */}

            <FormControl className={classes.formControl}>
              <InputLabel>Techlead name</InputLabel>
              <Select
                name="techleadname"
                value={values.techleadname}
                onChange={handleInputChange}
              >
                {techName.map((name) => {
                  return (
                    <MenuItem key={name.employeeId} value={name.employeeId}>
                      {name.firstName}
                    </MenuItem>
                  );
                })}
              </Select>
            </FormControl>

            <TextField
              id="date"
              label="Submission date"
              type="date"
              defaultValue="2017-05-24"
              name="date"
              onChange={handleInputChange}
            />
          </Grid>
        </Grid>

        <Paper className={classes.Paper1}>
          <Button
            variant="outlined"
            startIcon={<AddIcon />}
            className={classes.newbutton}
            onClick={() => setOpenPopup(true)}
          >
            {" "}
            Add New{" "}
          </Button>

          <ExpenseTable />
        </Paper>
        <Button
          type="submit"
          variant="contained"
          color="primary"
          className={classes.Button}
          onClick={handleClick}
        >
          Submit
        </Button>

        <Snackbar
          className={classes.Snackbar}
          anchorOrigin={{ vertical: "top", horizontal: "right" }}
          open={openNoti}
          autoHideDuration={3000}
          onClose={handleClose}
        >
          <Alert onClose={handleClose} severity="success">
            Form Submitted Succesfully
          </Alert>
        </Snackbar>

        <Button
          type="reset"
          onClick={resetForm}
          variant="contained"
          color="secondary"
          className={classes.Button}
        >
          Reset
        </Button>
      </form>

      <Popup openPopup={openPopup} setOpenPopup={setOpenPopup}></Popup>
    </div>
  );
}
