import {React,useState,useContext} from 'react'
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';


import {ContextState} from '../../../context/contexStates'



const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: "#EC2329",
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);



const useStyles = makeStyles({
  table: {
    minWidth: 700,
  },
});


export default function CustomizedTables() {
  const classes = useStyles();

  const {ERecords } = useContext(ContextState);
  const [ERecords1,setERecords1]  = ERecords


    const onDelete =id =>{
 
    const newRecords = ERecords1.filter(x=>x.receiptno !==id)
setERecords1(newRecords)
   }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="customized table">
        <TableHead>
          <TableRow>
            <StyledTableCell>Date</StyledTableCell>
            <StyledTableCell align="right">Discription</StyledTableCell>
            {/* <StyledTableCell align="right">Place</StyledTableCell> */}
            <StyledTableCell align="right">Receipt No</StyledTableCell>
            <StyledTableCell align="right">Receipt</StyledTableCell>
            <StyledTableCell align="right">Amount</StyledTableCell>
            <StyledTableCell align="right">Action</StyledTableCell>
           
          </TableRow>
        </TableHead>
        <TableBody>
          {ERecords1.map((row) => (
            <StyledTableRow key={row.receiptno}>
              <StyledTableCell component="th" scope="row">
                {row.date}
              </StyledTableCell>
             
              <StyledTableCell align="right">{row.description}</StyledTableCell>
           
              <StyledTableCell align="right">{row.receiptno}</StyledTableCell>
              <StyledTableCell align="right">{row.receipt}</StyledTableCell>
              <StyledTableCell align="right">{row.amount}</StyledTableCell>
              <StyledTableCell align="right"><DeleteForeverIcon
               color="secondary"
               onClick={()=> {
                 onDelete(row.receiptno)
                }}
              /></StyledTableCell>
            
              </StyledTableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}