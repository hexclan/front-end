import {Grid,  TextField, Button} from '@material-ui/core'

import {React,useState,useContext} from 'react'

import { makeStyles } from "@material-ui/core";

import {ContextState} from '../../../context/contexStates'






const initialFValues = {

   date: "",
   description:'',
   receiptno:"",
   billImage:'',
   amount:"",
   place:"" 


}



const useStyles = makeStyles(theme => ({
    root: {
        '& .MuiFormControl-root': {
            width: '80%',
            margin: theme.spacing(2)
        },

    },

    Button:{
        marginLeft:"9.5rem"
   
    }


}))

export default function PopupTableMecial(props) {


    const classes = useStyles();
    
    const [values, setValues] = useState(initialFValues);
  
    const[errors,setErrors]=useState({})

    const { ERecords,url } = useContext(ContextState);
    const [ERecords1,setERecords1]  = ERecords
    const[url1,SetUrl1]=url





    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('description' in fieldValues)
            temp.description = fieldValues.description ? "" : "This field is required."
            if ('place' in fieldValues)
            temp.place = fieldValues.place ? "" : "This field is required."
            if ('receiptno' in fieldValues)
            temp.receiptno = (/^[0-9]*$/).test(fieldValues.receiptno)&&fieldValues.receiptno? "" : "This field is required."
            // if ('receipt' in fieldValues)
            // temp.receipt = fieldValues.receipt ? "" : "This field is required."
            if ('amount' in fieldValues)
            temp.amount = (/^[0-9]*$/).test(fieldValues.amount)&&fieldValues.amount? "" : "This field is required."
       
        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }



    const handlePopChange = e => {
        const { name, value } = e.target
        setValues({
            ...values,
            [name]: value
        })

    }

  

    const handleReceipt = async(e) =>{
          
        const files=e.target.files
        const data=new FormData()
        data.append('file',files[0])
        data.append('upload_preset','my_upload')
        const res=await fetch('	https://api.cloudinary.com/v1_1/hexclanuom/image/upload',{
          method:'POST',
          body:data
        })
        const file =await res.json()
      
     
       
       setValues({
           ...values,
           billImage:file.secure_url
       })
        // SetUrl1(file.secure_url)
     
    

    }
    

    

    const handleSubmit =e=>{
    
        e.preventDefault();
      
        
        if(validate()){
     
        const allRecords =[...ERecords1,values];
            setERecords1(
                allRecords
            )
        
            setOpenPopup(false)
        }
       
    }



    const{openPopup,setOpenPopup} = props



    return (
        <div>
            <form className={classes.root} onSubmit={handleSubmit}>
                <Grid container>
                    <Grid item xs={12} sm={6}>

                        <TextField
                            variant="outlined"
                            name="date"
                            type="date"
                            label="Date"
                            defaultValue="2021-02-21"
                            onChange={handlePopChange}
                           
                        />
                        <TextField
                            variant="outlined"
                            name="description"
                       
                            label="Description"
                            value={values.description}
                            onChange={handlePopChange}
                            error={errors.description}

                        />
                        <TextField
                            variant="outlined"
                            name="place"
                       
                            label="Place"
                            value={values.place}
                            onChange={handlePopChange}
                            error={errors.place}
                        />
                   </Grid>

                    <Grid item xs={12} sm={6} >

                        <TextField
                            variant="outlined"
                            name="receiptno"
                            label="Receipt No"
                            value={values.receiptno}
                            onChange={handlePopChange}
                            error={errors.receiptno}
                        />


                        <TextField
                            variant="outlined"
                            name="receipt"
                            type="file"
                            // value={values.receipt}
                            onChange={handleReceipt}
                            // error={errors.receipt}

                        />

                            <TextField
                            variant="outlined"
                            name="amount"
                            label="Amount"
                            value={values.amount}
                            onChange={handlePopChange}
                            error={errors.amount}

                        />

<Button variant="contained" color="primary" className={classes.Button} type ="submit"
>
        Submit
      </Button>
      


                    </Grid>
                </Grid>
      </form>
        </div>


    )
}
