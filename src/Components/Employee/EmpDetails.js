import React ,{ useContext,useState,useEffect } from "react";
import {Link} from 'react-router-dom'
import styled from "styled-components";
import pro from "../../assets/pro.jpg"
import {EmpContext} from "../../context/ContextEmp"
import {GlobalContext} from "../../context/GlobalContext"

import {getProjects} from '../../services/employee'
import logo2 from "../../assets/logo2.svg";
import {addProfile, getProfileForEmployee, getProfiles,updateProfile} from '../../services/profilePicture'
export default function EmployeeCard() {
         const [Emp,SetEmp]  = useContext(EmpContext)
const { loggedInUser,signIn, dispatch } = useContext(GlobalContext);
const[image,setImage]=useState('')
const[isLoading,setIsLoading]=useState(false)
const[url,SetUrl]=useState('')
const [projects, setProjects] = useState([]);
const userDetails=JSON.parse(localStorage.getItem('user'))

 useEffect(async () => {
    getProfileForEmployee(userDetails.employee.employeeId)
      .then((res) => {
        setIsLoading(true)
        setImage(res);
        
        console.log(image);
      })
      .catch((err) => {
        console.log(err);
      });
      getProjects(userDetails.employee.employeeId)
      .then((res) => {
        setProjects(res)
        console.log(res)
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const uploadImage=async(e)=>{
    const files=e.target.files
    const data=new FormData()
    data.append('file',files[0])
    data.append('upload_preset','my_upload')
    const res=await fetch('	https://api.cloudinary.com/v1_1/hexclanuom/image/upload',{
      method:'POST',
      body:data
    })
    const file =await res.json()
    console.log(file)
    console.log(file.secure_url)
    SetUrl(file.secure_url)
    console.log(url)
  
   if(image[0]===null)
   {  const newProf={
     profilePicture:file.secure_url,
     employee:{
       employeeId:userDetails.employee.employeeId
     }
   }
   console.log(newProf)
     addProfile(newProf)
      .then((res) => {
        getProfiles()
          .then((res) => {
            setImage(res);
            console.log(res)
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  }else{
const newProf={
  profilePicId:image.profilePicId,
     profilePicture:file.secure_url,
     employee:{
       employeeId:userDetails.employee.employeeId
     }
   }
   console.log(newProf)
     updateProfile(newProf)
      .then((res) => {
        getProfiles()
          .then((res) => {
            setImage(res);
            console.log(res)
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  }
  return (
    <>
      <CardWrapper>
          
        <div class="container emp-profile">
          <form method="post">
            <div class="row">
              <div class="col-md-4">
                <div class="profile-img">
             { isLoading &&    < img src={image[0] !==null ?image.profilePicture:pro} alt="" />}
                  <div class="file btn btn-lg btn-primary">
                    Change Photo
                    <input type="file" name="file" onChange={uploadImage} />
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="profile-head text-capitalize">
                  <h5>{`${userDetails.employee.firstName} ${userDetails.employee.lastName}`}</h5>
                  <h6>{userDetails.employee.designation.designation}</h6>

                  <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <div class="nav-link active" id="home-tab">
                        About
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="col-md-2 text-capitalize">
                <Link to={`/employee/edit/${userDetails.employee.employeeId}`}>
                <input
                  type="submit"
                  class="profile-edit-btn"
                  name="btnAddMore"
                  value="Edit Profile"
                /></Link>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 text-capitalize">
                <div class="profile-work">
                  <h4>Projects</h4>
                 {projects.map((pro) => {
                        return (
                          <p >
                           { pro.projectName}
                          </p>
                        );
                      })}
                  <br />
                  

                  <br />
                </div>
              </div>
              <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                  <div
                    class="tab-pane fade show active"
                    id="home"
                    role="tabpanel"
                    aria-labelledby="home-tab"
                  >
                    <div class="row">
                      <div class="col-md-6">
                        <label>Employee Id</label>
                      </div>
                      <div class="col-md-6">
                        <p>{userDetails.employee.employeeId}</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6 ">
                        <label>Address</label>
                      </div>
                      <div class="col-md-6 text-capitalize">
                        <p>{userDetails.employee.address}</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Email</label>
                      </div>
                      <div class="col-md-6">
                        <p>{userDetails.employee.email}</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Phone</label>
                      </div>
                      <div class="col-md-6">
                        <p>{userDetails.employee.phoneNo}</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Department</label>
                      </div>
                      <div class="col-md-6 text-capitalize">
                        <p>{userDetails.employee.department.department}</p>
                      </div>
                    </div>
                  </div>
                  <div
                    class="tab-pane fade"
                    id="profile"
                    role="tabpanel"
                    aria-labelledby="profile-tab"
                  >
                    <div class="row">
                      <div class="col-md-6">
                        <label>Experience</label>
                      </div>
                      <div class="col-md-6">
                        <p>Expert</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Hourly Rate</label>
                      </div>
                      <div class="col-md-6">
                        <p>10$/hr</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Total Projects</label>
                      </div>
                      <div class="col-md-6">
                        <p>230</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>English Level</label>
                      </div>
                      <div class="col-md-6">
                        <p>Expert</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <label>Availability</label>
                      </div>
                      <div class="col-md-6">
                        <p>6 months</p>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <label>Your Bio</label>
                        <br />
                        <p>Your detail description</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </CardWrapper>
    </>
  );
}

const CardWrapper = styled.div`
  padding-left: 4.5rem;
  img {
    width: 100px;
    height: 100px;
  }
  body {
    background: -webkit-linear-gradient(left, #3931af, #00c6ff);
  }
  .emp-profile {
    padding: 3%;
    margin-left:10%;
    margin-bottom: 3%;
    border-radius: 0.5rem;
    background: #fff;
  }
  .profile-img {
    text-align: center;
  }
  .profile-img img {
    width: 70%;
    height: 100%;
  }
  .profile-img .file {
    position: relative;
    overflow: hidden;
    margin-top: -20%;
    width: 70%;
    border: none;
    border-radius: 0;
    font-size: 15px;
    background: #212529b8;
  }
  .profile-img .file input {
    position: absolute;
    opacity: 0;
    right: 0;
    top: 0;
  }
  .profile-head h5 {
    color: #333;
  }
  .profile-head h6 {
    color: #0062cc;
  }
  .profile-edit-btn {
    border: none;
    border-radius: 1.5rem;
    width: 70%;
    padding: 2%;
    font-weight: 600;
    color: #6c757d;
    cursor: pointer;
  }
  .proile-rating {
    font-size: 12px;
    color: #818182;
    margin-top: 5%;
  }
  .proile-rating span {
    color: #495057;
    font-size: 15px;
    font-weight: 600;
  }
  .profile-head .nav-tabs {
    margin-bottom: 5%;
  }
  .profile-head .nav-tabs .nav-link {
    font-weight: 600;
    border: none;
  }
  .profile-head .nav-tabs .nav-link.active {
    border: none;
    border-bottom: 2px solid #0062cc;
  }
  .profile-work {
    padding: 14%;
    margin-top: -15%;
  }
  .profile-work p {
    font-size: 12px;
    color: #818182;
    font-weight: 600;
    margin-top: 10%;
  }
  .profile-work a {
    text-decoration: none;
    color: #495057;
    font-weight: 600;
    font-size: 14px;
  }
  .profile-work ul {
    list-style: none;
  }
  .profile-tab label {
    font-weight: 600;
  }
  .profile-tab p {
    font-weight: 600;
    color: #0062cc;
  }
`;




// import React from 'react'
// import Grid from "@material-ui/core/Grid";
// import Avatar from '@material-ui/core/Avatar';
// import { makeStyles } from '@material-ui/styles'
// import {useTheme} from '@material-ui/core/styles'
// import pro from "../assets/pro.jpg"
// import Container from '@material-ui/core/Container';
// import Typography from '@material-ui/core/Typography';


// const useStyles = makeStyles(theme => ({ 

//     maindiv:{
//         marginLeft:"240px",
        
//         [theme.breakpoints.down("sm")]: {
//             marginLeft:"0px",
//           },
//     },
//     avatar: {

//         width: theme.spacing(8),
//         height: theme.spacing(8),
        
       
    
    
//       },
//       alignItemsAndJustifyContent: {
//         width: 500,
//         height: 80,
//         display: 'flex',
       
//         justifyContent: 'center',
        
//       },
//       griditem:{
//           margin:"1em"
//       }

// }))


// export default function EmpDetails() {
//     const classes = useStyles()
//     const theme=useTheme();


//     return (
//         <div  className={classes.maindiv} >
//         <Grid container direction="column" >
//             <Container>
//             <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '20vh' }} >
// <Grid item>
//         <Grid
//   container
//   direction="row"
//   justify="flex-start"
//   style={{ marginLeft:100 ,marginTop:50}}
//   alignItems="center"
//   spacing={4}
// >
//              <Grid item > 
//              <Avatar className={classes.avatar}  src={pro} />        
//              </Grid>
//              <Grid item >
//                Mr.Kumara.L.D.K <br/> 183123K
//              </Grid>
//              <Grid item style={{ marginLeft:"10em"}} xs={12} sm={6} >
//                <a href="" >manage your profile</a>
//              </Grid>

//          </Grid>
//          </Grid>
//          </Typography>
//          </Container>
//         <Grid item style={{marginTop:30 }} className={classes.alignItemsAndJustifyContent}>
//             <Container  maxWidth={"md"}>
//             <Typography component="div" style={{ backgroundColor: '#cfe8fc' }} >
            

//             <Grid container direction="column"  >
//             <Grid container className={classes.griditem} >
//                 <Grid item >
//                 <b> User Details</b>
//                 </Grid>
//                 </Grid>
//                 <Grid item>
//                     <Grid container  >
//                         <Grid className={classes.griditem}>
//                         Mail Address <br/>
//                          kumara@creative.info
//                         </Grid>
//                         <Grid className={classes.griditem}>
//                             Country <br/>
//                             India
//                         </Grid>
//                     </Grid>
//                 </Grid>
//                 <Grid item>
//                     <Grid container   >
//                         <Grid className={classes.griditem}>
//                         Contact No <br/>
//                        +9132344322
//                         </Grid>
//                         <Grid className={classes.griditem} >
//                             City/Town <br/>
//                             Chennai
//                         </Grid>
//                     </Grid>
//                 </Grid>

//             </Grid>

//             </Typography>
           
//            </Container>
//         </Grid>

//         <Grid item style={{marginTop:200}}  className={classes.alignItemsAndJustifyContent}>
//             <Container>
//             <Typography component="div" style={{ backgroundColor: '#cfe8fc', height: '20vh' }} >
//             <Grid container direction="column" spacing={3}>
//               <Grid item>
//                 <b> Reports</b>
//               </Grid>
// <Grid item>
// Broswer sessions <br/>
// project overview
// </Grid>
//             </Grid>
//             </Typography>
//             </Container>
//         </Grid>


//          </Grid>

//         </div>
//     )
// }
