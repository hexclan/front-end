import React, { useContext, useEffect, useState } from "react";
import { AdminContext } from "../../context/adminContext";
import { Link, useHistory } from "react-router-dom";
import styled from "styled-components";
import pro from "../../assets/pro.jpg";
import { getDependentForEmployee } from "../../services/user";

const ViewUser = (props) => {
  const { employees } = useContext(AdminContext);
  const [dependents, setDependents] = useState([]);
  const id = props.match.params.id;
  const employee = employees.find((employee) => employee.employeeId == id);

  const {
    employeeId,
    firstName,
    lastName,
    email,
    phoneNo,
    datetime,
    address,
  } = employee;
  const { department } = employee.department;
  const { designation } = employee.designation;
  const { roleName } = employee.userRole[0];

  const history = useHistory();
  const editHandler = () => {
    history.push(`/admin/edit-user/${id}`);
  };

  console.log(dependents);

  useEffect(async () => {
    getDependentForEmployee(id)
      .then((res) => {
        setDependents(res);
        console.log(dependents);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <Styles>
      <Link to={"/admin"}>
        <i className="fas fa-arrow-circle-left back"></i>
      </Link>
      <i
        className="fas fa-user-edit text-danger edit"
        onClick={editHandler}
      ></i>

      <div class="container">
        <div className="row">
          <div class="col-2">
            {" "}
            <img src={pro} alt="" />
          </div>
          <div class="col-10">
            <h3>
              {firstName} {lastName}
            </h3>
          </div>
        </div>
        <div class="row">
          <div class="col-2"> Employee ID </div>
          <div class="col-10">{employeeId}</div>
        </div>
        <div class="row">
          <div class="col-2"> Role </div>
          <div class="col-10">{roleName}</div>
        </div>
        <div class="row">
          <div class="col-2"> Email </div>
          <div class="col-10">{email}</div>
        </div>
        <div class="row">
          <div class="col-2"> Phone Number </div>
          <div class="col-10">{phoneNo}</div>
        </div>
        <div class="row">
          <div class="col-2"> Date of Birth </div>
          <div class="col-10">{datetime}</div>
        </div>
        <div class="row">
          <div class="col-2"> Address </div>
          <div class="col-10">{address}</div>
        </div>
        <div class="row">
          <div class="col-2"> Designation </div>
          <div class="col-10">{designation}</div>
        </div>
        <div class="row">
          <div class="col-2"> Department </div>
          <div class="col-10">{department}</div>
        </div>
        <br />
        <div class="row">
          <div class="col-4">
            <h5>Dependents {dependents.length !== 0 ? " " : ": none"}</h5>
          </div>
        </div>

        {dependents.map((dependent) => {
          return (
            <>
              <div className="row">
                <div class="col-2"> Dependent ID </div>
                <div class="col-10">{dependent.dependentId}</div>
              </div>
              <div className="row">
                <div class="col-2"> Name </div>
                <div class="col-10">{dependent.dependentName}</div>
              </div>
              <div className="row">
                <div class="col-2"> Relationship </div>
                <div class="col-10">{dependent.relationship}</div>
              </div>
              <div className="row">
                <div class="col-2"> Date of Birth </div>
                <div class="col-10">{dependent.dob}</div>
              </div>
              <div className="row">
                <div class="col-2"> Gender </div>
                <div class="col-10">{dependent.gender}</div>
              </div>
              <br />
            </>
          );
        })}
      </div>
    </Styles>
  );
};

const Styles = styled.div`
  padding-top: 2rem;
  margin-left: 15rem;

  .back {
    color: red;
    font-size: 30px;
  }

  .edit {
    font-size: 30px;
    font-color: blue;
  }
`;

export default ViewUser;
