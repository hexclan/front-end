import React, { useContext } from "react";
import { Link, useHistory } from "react-router-dom";
import { AdminContext } from "../../context/adminContext";
import { Formik, Field, Form, ErrorMessage } from "formik";
import Validation from "./UserValidation";
import { updateEmployee } from "../../services/user";
import { getUsers } from "../../services/user";
export default function EditUser(props) {
  const history = useHistory();

  const {
    employees,
    setEmployees,
    departments,
    designations,
    roles,
  } = useContext(AdminContext);

  const id = props.match.params.id;
  const newEmployee = employees.find((employee) => employee.employeeId == id);
  const {
    employeeId,
    firstName,
    lastName,
    email,
    phoneNo,
    datetime,
    address,
  } = newEmployee;

  const { departmentId } = newEmployee.department;
  const { designationId } = newEmployee.designation;
  const { roleId } = newEmployee.userRole[0];

  const initialValues = {
    firstName,
    lastName,
    designationID: designationId,
    departmentID: departmentId,
    roleID: roleId,
    phoneNo,
    datetime: datetime,
    address: address,
  };

  const onSubmit = (fields) => {};

  const handleSubmitting = (val) => {
    const {
      firstName,
      lastName,
      roleID,
      designationID,
      departmentID,
      phoneNo,
      datetime,
      address,
    } = val;

    let userRoleArr = [{ roleId: "1" }];
    if (roleID !== 1) {
      const obj = { roleId: roleID };
      userRoleArr = [...userRoleArr, obj];
    }

    const newEmp = {
      employeeId,
      datetime,
      address,
      firstName,
      lastName,

      email,
      phoneNo,
      department: {
        departmentId: departmentID,
      },
      designation: {
        designationId: designationID,
      },
      userRole: userRoleArr,
    };

    updateEmployee(newEmp)
      .then((res) => {
        getUsers()
          .then((res) => {
            setEmployees(res);
            history.push("/admin");
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="container">
      <div className="row">
        <div className="card col-md-10 offset-md-2 offset-md-2">
          <h5 className="text-center">Edit User</h5>
          <div className="card-body">
            <Formik
              initialValues={initialValues}
              validationSchema={Validation}
              onSubmit={onSubmit}
            >
              {({ values }) => (
                <Form style={{ display: "flex", flexDirection: "column" }}>
                  <div className="col-6">
                    <label className="form-label">First Name</label>
                    <Field
                      name="firstName"
                      type="text"
                      placeholder="First Name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="firstName"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <div className="col-6">
                    <label className="form-label">Last Name</label>
                    <Field
                      name="lastName"
                      type="text"
                      placeholder="Last Name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="lastName"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  {/*<div className="col-6">
                    <label className="form-label">Employee ID</label>
                    <Field
                      name="employeeId"
                      type="text"
                      placeholder="Employee ID"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="employeeId"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>*/}
                  <div className="col-6">
                    <label className="form-label">Designation</label>
                    <Field
                      name="designationID"
                      component="select"
                      className="form-control"
                    >
                      <option value="" disabled selected hidden>
                        Please Choose...
                      </option>
                      {designations.map((desig) => {
                        return (
                          <option value={desig.designationId}>
                            {desig.designation}
                          </option>
                        );
                      })}
                    </Field>
                  </div>
                  <div className="col-6">
                    <label className="form-label">Department</label>
                    <Field
                      name="departmentID"
                      component="select"
                      className="form-control"
                    >
                      <option value="" disabled selected hidden>
                        Please Choose...
                      </option>
                      {departments.map((dep) => {
                        return (
                          <option value={dep.departmentId}>
                            {dep.department}
                          </option>
                        );
                      })}
                    </Field>
                  </div>
                  <div className="col-6">
                    <label className="form-label">Role</label>
                    <Field
                      name="roleID"
                      component="select"
                      className="form-control"
                    >
                      <option value="" disabled selected hidden>
                        Please Choose...
                      </option>
                      {roles.map((role) => {
                        return (
                          <option value={role.roleId}>{role.roleName}</option>
                        );
                      })}
                    </Field>
                  </div>
                  {/* <div className="col-6">
                    <label className="form-label">Email</label>
                    <Field
                      name="email"
                      type="email"
                      placeholder="Email"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="email"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div> */}
                  <div className="col-6">
                    <label className="form-label">Phone Number</label>
                    <Field
                      name="phoneNo"
                      type="text"
                      placeholder="phone number"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="phoneNo"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <div className="col-6">
                    <label className="form-label">Address</label>
                    <Field
                      name="address"
                      type="text"
                      placeholder="address"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="address"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <div className="col-6">
                    <label className="form-label">Date of birth</label>
                    <Field
                      name="datetime"
                      type="date"
                      placeholder="Birth Date"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="datetime"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <div className="col-12">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => handleSubmitting(values)}
                    >
                      Update
                    </button>
                    <Link to={"/admin"}>
                      <button className="btn btn-danger">Cancel</button>{" "}
                    </Link>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
