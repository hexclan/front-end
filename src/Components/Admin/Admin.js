import React, { useContext, useEffect, useState } from "react";
import { AdminContext } from "../../context/adminContext";
import styled from "styled-components";
import { deleteEmployee, getUsers } from "../../services/user";
import { Link } from "react-router-dom";
import ConfirmationPopup from "./ConfirmationPopup";

const Styles = styled.div`
  // padding-top: 2rem;
  margin-left: 15rem;

  .sI {
    color: red;
    font-size: 20px;
    margin-left: 10px;
  }

  .mI {
    color: red;
    font-size: 30px;
  }

  table {
    margin: 25px;
  }

  table tr {
    margin: 5px;
  }
`;

const Admin = () => {
  const { employees, setEmployees } = useContext(AdminContext);

  const [openPopup, setOpenPopup] = useState(false);
  const [decision, setDecision] = useState(false);
  const [idtodlt, setIdtodlt] = useState(null);

  employees.sort((a, b) => {
    return a.employeeId - b.employeeId;
  });

  const deleteUser = (id) => {
    setOpenPopup(true);
    setIdtodlt(id);
    console.log(decision);
    // if (decision) {
    //   deleteEmployee(id)
    //     .then((res) => {
    //       getUsers()
    //         .then((res) => {
    //           setEmployees(res);
    //         })
    //         .catch((err) => {
    //           console.log(err);
    //         });
    //     })
    //     .catch((err) => {
    //       console.log(err);
    //       setDecision(false);
    //     });
    // }
  };

  useEffect(() => {
    if (decision) {
      deleteEmployee(idtodlt)
        .then((res) => {
          setDecision(false);
          setIdtodlt(null);
          getUsers()
            .then((res) => {
              setEmployees(res);
            })
            .catch((err) => {
              console.log(err);
            });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [decision]);

  return (
    <Styles>
      {/* <div>
        <button onClick={() => setOpenPopup(true)}> Add New </button>
      </div> */}

      <div className="container mt-5">
        <Link to={"/admin/add-user"}>
          <i className="fas fa-user-plus text-danger mI"></i>
        </Link>
      </div>
      <table className="table">
        <thead>
          <th scope="col">Employee Id</th>
          <th scope="col">Full Name</th>
          <th scope="col">Email</th>
          <th scope="col">Role</th>
          <th scope="col">Actions</th>
        </thead>
        <tbody>
          {employees.map((emp) => {
            return (
              <tr>
                <td>{emp.employeeId}</td>
                <td>
                  {emp.firstName} {emp.lastName}
                </td>
                <td>{emp.email}</td>
                <td>{emp.userRole[0].roleName}</td>
                <td>
                  <Link to={`/admin/edit-user/${emp.employeeId}`}>
                    <i className="fas fa-user-edit text-dark sI"></i>
                  </Link>
                  {/* </td>
                <td> */}
                  <i
                    className="fas fa-user-times text-danger sI"
                    onClick={() => deleteUser(emp.employeeId)}
                  ></i>
                  {/* </td>
                <td> */}
                  <Link to={`/admin/view-user/${emp.employeeId}`}>
                    <i className="fas fa-mouse-pointer text-info sI"></i>
                  </Link>
                  {/* </td>
                <td> */}
                  <Link to={`/admin/add-dependent/${emp.employeeId}`}>
                    <i className="fas fa-child text-dark sI"></i>
                  </Link>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <ConfirmationPopup
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        decision={decision}
        setDecision={setDecision}
      ></ConfirmationPopup>
    </Styles>
  );
};

export default Admin;
