import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Formik, Field, Form, ErrorMessage } from "formik";
import Validation from "./UserValidation";
import { addDependent } from "../../services/user";

export default function AddDependent(props) {
  const history = useHistory();
  const id = props.match.params.id;

  const initialValues = {
    dependentName: "",
    relationship: "",
    gender: "",
    dob: "",
  };

  const onSubmit = (fields) => {};

  const handleSubmitting = (val) => {
    const { dependentName, relationship, gender, dob } = val;

    const newDependent = {
      dependentName,
      relationship,
      gender,
      dob,
      employee: {
        employeeId: id,
      },
    };

    addDependent(newDependent)
      .then((res) => {
        history.push("/admin");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <div className="container">
      <div className="row">
        <div className="card col-md-10 offset-md-2 offset-md-2">
          <h5 className="text-center">Add Dependent</h5>

          <div className="card-body">
            <Formik
              initialValues={initialValues}
              validationSchema={Validation}
              onSubmit={onSubmit}
            >
              {({ values }) => (
                <Form style={{ display: "flex", flexDirection: "column" }}>
                  <div className="col-6">
                    <label className="form-label">Dependent Name</label>
                    <Field
                      name="dependentName"
                      type="text"
                      placeholder="Dependent Name"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="dependentName"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>

                  <div className="col-6">
                    <div className="row">
                      <div className="col-3">
                        <label className="form-label">Relationship</label>
                      </div>
                      <div className="col-2">
                        <Field
                          type="radio"
                          name="relationship"
                          value="partner"
                          className="form-check-input"
                        />
                        <label className="form-check-label">Partner</label>
                      </div>
                      <div className="col-2">
                        <label className="form-check-label">
                          <Field
                            type="radio"
                            name="relationship"
                            value="child"
                            className="form-check-input"
                          />
                          Child
                        </label>
                      </div>
                    </div>
                  </div>

                  <div className="col-6">
                    <div className="row">
                      <div className="col-3">
                        <label className="form-label">Gender</label>
                      </div>
                      <div className="col-2">
                        <Field
                          type="radio"
                          name="gender"
                          value="male"
                          className="form-check-input"
                        />
                        <label className="form-check-label">Male</label>
                      </div>
                      <div className="col-2">
                        <label className="form-check-label">
                          <Field
                            type="radio"
                            name="gender"
                            value="female"
                            className="form-check-input"
                          />
                          Female
                        </label>
                      </div>
                      <div className="col-2">
                        <label className="form-check-label">
                          <Field
                            type="radio"
                            name="gender"
                            value="other"
                            className="form-check-input"
                          />
                          Other
                        </label>
                      </div>
                    </div>
                  </div>

                  <div className="col-6">
                    <label className="form-label">Date of birth</label>
                    <Field
                      name="dob"
                      type="date"
                      placeholder="Birth Date"
                      className="form-control"
                    />
                    <ErrorMessage
                      name="dob"
                      component="div"
                      style={{ color: "red", fontWeight: "bold" }}
                    />
                  </div>
                  <div className="col-12">
                    <button
                      className="btn btn-primary"
                      type="button"
                      onClick={() => handleSubmitting(values)}
                    >
                      Submit
                    </button>
                    <Link to={"/admin"}>
                      <button className="btn btn-danger">Cancel</button>{" "}
                    </Link>
                  </div>
                </Form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
}
