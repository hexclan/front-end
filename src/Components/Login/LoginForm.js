import React, { Component, useContext, useState } from "react";
import { Button } from "react-bootstrap";
import "../Login/LoginForm.css";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";

//import { useHistory } from 'react-router-dom';
//import {Link} from "react-router-dom";
//import {Redirect} from 'react-router-dom';
import { Link, useHistory } from "react-router-dom";
import { GlobalContext } from "../../context/GlobalContext";
import { signInUser } from "../../services/auth/authToken";

export const LoginForm = (props) => {
  const { userDetails, loggedInUser, signIn, dispatch } = useContext(
    GlobalContext
  );
  
  const history = useHistory();

  const [{ email, password }, setState] = useState({
    email: "",
    password: "",
  });

  const onSubmit = async (e) => {
    e.preventDefault();

    try {
      const user = await signInUser({ email, password });
            console.log(user)
   //   dispatch(signIn(user));

      history.replace({ pathname: "/select" });
    } catch (error) {
      alert(error);
    }
    console.log(loggedInUser)
  };

  const onChange = (e) => {
    const { name, value } = e.target;

    setState((prevState) => ({ ...prevState, [name]: value }));
  };

  return (
    <div className="bak">
      <div className="flex-container">
        <div className="logo-container">
          {/* <img src={logo} className="App-logo" alt="logo" /> */}
        </div>
        {/* <Divider
                orientation="vertical"
                style={{ color: "black", width: "2px", height: '60%' }}
              /> */}

        <div>
          <form className="form">
            <h1 className="h1">T Y C H E</h1>
            <br />
            <br />
            <h3>Welcome to Tyche</h3>
            <br />

            <Grid item>
              <TextField
                label="Username"
                name="email"
                type="text"
                variant="standard"
                value={email}
                onChange={onChange}
                placeholder="Enter email address"
                required
              />
            </Grid>

            <br />
            <Grid item>
              <TextField
                label="Password"
                name="password"
                type="password"
                variant="standard"
                value={password}
                onChange={onChange}
                placeholder="Enter password"
                required
              />
            </Grid>

            <br />

            <Button
              varient="success"
              disabled={email.length === 0 || password.length === 0}
              onClick={onSubmit}
            >
              Login
            </Button>

            <div>
              <a href={"url"}>forget password?</a>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};
