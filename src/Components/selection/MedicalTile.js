


//import Grid from "@material-ui/core/Grid";
import React from 'react'
import { makeStyles } from '@material-ui/styles'
//import { ThemeProvider } from '@material-ui/styles'
//import Header from "./Components/Header"
//import theme from "./Components/Theme";
//import Sidebar from "./Sidebar"
//import expense from "./Components/Expense"
import Paper from '@material-ui/core/Paper';
//import { BrowserRouter ,Route,Switch} from 'react-router-dom'
//import back from "../back.svg"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faLaptopMedical } from '@fortawesome/free-solid-svg-icons'
import { faArrowAltCircleRight } from '@fortawesome/free-solid-svg-icons'
import Typography from "@material-ui/core/Typography"
import { faBriefcase } from '@fortawesome/free-solid-svg-icons'
import {Link} from "react-router-dom"
//import back from "../back.svg"
import {useTheme} from '@material-ui/core/styles'


const useStyles = makeStyles(theme => ({
  toolbarMargin: {
    ...theme.mixins.toolbar
  },

  root: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingTop: "0rem",
    paddingLeft: "0rem",
   
    marginLeft: "0px",
    borderRadius: 3,
    '& > *': {
      margin: theme.spacing(3),
      width: theme.spacing(40),
      height: theme.spacing(50),
    
    },
    [theme.breakpoints.down("sm")]: {
      marginLeft:"-23rem",
      paddingLeft: "0rem",
    },
   
  },

  paper1: {
    borderRadius: "48px",
   justifyItems:"center",
   boxShadow: '18px 3px 15px 0 rgba(245, 224, 224, 20)',
   opacity: 0.9,
   "&:hover": {
    textDecoration:"none",
    color:"black",
    opacity:1,
    
  }
},

  lap:{
        marginTop:"4rem",
        marginLeft:"6.5rem",
        color:"#FAB615"
  },
  suit:{
    marginTop:"3rem",
    marginLeft:"7.3rem",
    color:"#F58021"
  },
  typo: {
    fontFamily: "Roboto",
    fontWeight: 700,
    fontSize:"1.4rem",
    marginTop:"1.2rem",
   
  },
  arrow:{
    marginTop:"4rem",
        marginLeft:"9rem",
        color:"#388C7C"
  },
}))



export default function Select() {

  const classes = useStyles()
  const theme=useTheme();


  return (

    <div className={classes.root} >
       
      <Paper
        className={classes.paper1}
        elevation={3}
        component={Link} to="/medical"
        disableRipple
      >
        
        <FontAwesomeIcon className={classes.lap} icon={faLaptopMedical} size="6x" />
       <Typography 
       
       align="center"
        className={classes.typo}
        >Medical Reimbursement
        </Typography>
         
          
           
        <FontAwesomeIcon  className={classes.arrow} icon={faArrowAltCircleRight} size="2x" />
        
        </Paper>

      
    </div>
  
  )}
