import React from "react";
import { Link } from "react-router-dom";

export default function ReqTable({ requests, url, changeStatus, type }) {
  return (
    <div>
      <table className="table">
        <thead>
          <th scope="col">Request No</th>
          <th scope="col">Expense Type</th>
          <th scope="col">Employee</th>
          <th scope="col">Status</th>
          <th scope="col">date</th>
          <th scope="col">Amount</th>
          <th scope="col">Actions</th>
        </thead>
        <tbody>
          {requests
            .filter(
              (request) => request.progressStatus === "accepted by tech lead"
            )
            .map((req) => {
              return (
                <tr>
                  {type === "expense" ? (
                    <td>{req.expenseFormId}</td>
                  ) : (
                    <td>{req.medicalFormId}</td>
                  )}
                  <td>{req.formDescription}</td>
                  <td>
                    {req.employee.firstName} {req.employee.lastName}
                  </td>
                  <td>{req.progressStatus}</td>
                  <td>{req.submittedDate}</td>
                  <td>{req.amount}</td>
                  <td>
                    {type === "expense" ? (
                      <>
                        <i
                          className="fas fa-check text-success"
                          onClick={() =>
                            changeStatus(req.expenseFormId, 1, type)
                          }
                        ></i>
                        <i
                          className="fas fa-times text-danger"
                          onClick={() =>
                            changeStatus(req.expenseFormId, -1, type)
                          }
                        ></i>
                        <Link to={`${url}/${req.expenseFormId}`}>
                          <i className="fas fa-mouse-pointer text-danger"></i>
                        </Link>
                      </>
                    ) : (
                      ""
                    )}
                    {type === "medical" ? (
                      <>
                        <i
                          className="fas fa-check text-success"
                          onClick={() =>
                            changeStatus(req.medicalFormId, 1, type)
                          }
                        ></i>
                        <i
                          className="fas fa-times text-danger"
                          onClick={() =>
                            changeStatus(req.medicalFormId, -1, type)
                          }
                        ></i>
                        <Link to={`${url}/${req.medicalFormId}`}>
                          <i className="fas fa-mouse-pointer text-danger"></i>
                        </Link>
                      </>
                    ) : (
                      ""
                    )}
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    </div>
  );
}
