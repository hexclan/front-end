import React, { useContext } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { AccountantContext } from "../../context/accountantContext";

export default function ReportsDetails(props) {
  const id = props.match.params.id;
  const { requests, medicalRequests } = useContext(AccountantContext);

  let type = "expense";
  let selectedRequest = {};
  selectedRequest = requests.find((request) => request.expenseFormId == id);
  if (typeof selectedRequest === "undefined") {
    type = "medical";
    selectedRequest = medicalRequests.find(
      (request) => request.medicalFormId == id
    );
  }
  const {
    formDescription,
    progressStatus,
    submittedDate,
    amount,
  } = selectedRequest;
  const { firstName, lastName } = selectedRequest.employee;

  let receipt = [];
  if(type === "medical"){
    receipt = selectedRequest.receipt
  }else{
    receipt = selectedRequest.image
  }

  let formId = "";
  if (type === "expense") {
    formId = selectedRequest.expenseFormId;
  } else {
    formId = selectedRequest.medicalFormId;
  }

  return (
    <Styles>
      <div className="container">
        <div className="row">
          <div className="col-2">
            <Link to="/accountant/reports">
              <button className="btn btn-danger">Go back</button>
            </Link>
          </div>
          <div className="col-6">
            <h3>Details</h3>
          </div>
        </div>
        <div className="container content">
          <div className="row">
            <div className="col-2"> Employee </div>
            <div className="col-10">
              {firstName} {lastName}
            </div>
          </div>

          <div className="row">
            <div className="col-2"> Request No </div>
            <div className="col-10">{formId}</div>
          </div>

          <div className="row">
            <div className="col-2"> Expense Type </div>
            <div className="col-10">{formDescription}</div>
          </div>
          <div className="row">
            <div className="col-2"> progressStatus </div>
            <div className="col-10">{progressStatus}</div>
          </div>
          <div className="row">
            <div className="col-2"> submittedDate </div>
            <div className="col-10">{submittedDate}</div>
          </div>
          <div className="row">
            <div className="col-2"> Amount </div>
            <div className="col-10">{amount}</div>
          </div>
          <div>
            <h3>Receipts</h3>
            {receipt.map(rec => {
            return(
              <div key={rec.receiptId}>
                {rec.receiptId}<br/>
                {rec.amount}<br/>
                {rec.description}<br/>
                <img src={rec.receiptImage} alt={rec.receiptId}/><br/>
                </div>
            )
          })}
          </div>
        </div>
      </div>
    </Styles>
  );
}

const Styles = styled.div`
  padding-top: 5rem;
  margin-left: 20rem;

  .content {
    margin-top: 3rem;
    margin-left: 5rem;
  }

  .row {
    margin-bottom: 1rem;
  }
`;
