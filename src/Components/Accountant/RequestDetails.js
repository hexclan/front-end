import React, { useContext } from "react";
import { AccountantContext } from "../../context/accountantContext";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import { updateExpenseClaim } from "../../services/ExpenseForm";
import { updateMedicalClaim } from "../../services/MedicalForm";

export default function RequestDetails(props) {
  const id = props.match.params.id;
  let urlPart = props.match.path.replace(":id", "");
  const history = useHistory();
  const {
    requests,
    setRequests,
    medicalRequests,
    setMedicalRequests,
  } = useContext(AccountantContext);
  let type = "expense";
  let selectedRequest = {};
  selectedRequest = requests.find((request) => request.expenseFormId == id);
  if (typeof selectedRequest === "undefined") {
    type = "medical";
    selectedRequest = medicalRequests.find(
      (request) => request.medicalFormId == id
    );
  }

  const {
    formDescription,
    progressStatus,
    submittedDate,
    amount,
  } = selectedRequest;
  const { firstName, lastName } = selectedRequest.employee;

  let receipt = [];
  if(type === "medical"){
    receipt = selectedRequest.receipt
  }else{
    receipt = selectedRequest.image
  }
  

  let formId = "";
  if (type === "expense") {
    formId = selectedRequest.expenseFormId;
  } else {
    formId = selectedRequest.medicalFormId;
  }

  const changeStatus = (act) => {
    if (type === "expense") {
      let changedRequest = requests.find(
        (request) => request.expenseFormId === formId
      );

      if (act === 1) {
        changedRequest.progressStatus = "accepted by accountant";
      } else {
        changedRequest.progressStatus = "rejected by accountant";
      }

      updateExpenseClaim(changedRequest)
        .then((res) => {
          const filteredItems = requests.filter(
            (request) => request.expenseFormId !== formId
          );
          const newItems = [...filteredItems, res];
          setRequests(newItems);
          history.push("/accountant/requests");
        })
        .catch((err) => {
          console.log(err);
        });
    } else if (type === "medical") {
      let changedRequest = medicalRequests.find(
        (request) => request.medicalFormId === formId
      );

      if (act === 1) {
        changedRequest.progressStatus = "accepted by accountant";
      } else {
        changedRequest.progressStatus = "rejected by accountant";
      }

      updateMedicalClaim(changedRequest)
        .then((res) => {
          const filteredItems = medicalRequests.filter(
            (request) => request.medicalFormId !== formId
          );
          const newItems = [...filteredItems, res];
          setMedicalRequests(newItems);
          history.push("/accountant/requests");
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  return (
    <Styles>
      <div className="container">
        <div className="row">
          <div className="col-2">
            <Link to={urlPart}>
              <button className="btn btn-danger">Go back</button>{" "}
            </Link>
          </div>
          <div className="col-6">
            <h3>Details</h3>
          </div>
          {props.match.path == "/accountant/requests/:id" ? (
            <div className="col-4">
              <i
                className="fas fa-check text-success"
                onClick={() => changeStatus(1)}
              ></i>
              <i
                className="fas fa-times text-danger"
                onClick={() => changeStatus(-1)}
              ></i>
            </div>
          ) : (
            <div className="col-4"></div>
          )}
        </div>

        <div className="container content">
          <div className="row">
            <div className="col-2"> Employee </div>
            <div className="col-10">
              {firstName} {lastName}
            </div>
          </div>

          <div className="row">
            <div className="col-2"> Request No </div>
            <div className="col-10">{formId}</div>
          </div>

          <div className="row">
            <div className="col-2"> Expense Type </div>
            <div className="col-10">{formDescription}</div>
          </div>
          <div className="row">
            <div className="col-2"> progressStatus </div>
            <div className="col-10">{progressStatus}</div>
          </div>
          <div className="row">
            <div className="col-2"> submittedDate </div>
            <div className="col-10">{submittedDate}</div>
          </div>
          <div className="row">
            <div className="col-2"> Amount </div>
            <div className="col-10">{amount}</div>
          </div>
          <div>
            <h3>Receipts</h3>
            {receipt.map(rec => {
            return(
              <div key={rec.receiptId}>
                {rec.receiptId}<br/>
                {rec.amount}<br/>
                {rec.description}<br/>
                <img src={rec.receiptImage} alt={rec.receiptId}/><br/>
                </div>
            )
          })}
          </div>
        </div>
      </div>
    </Styles>
  );
}

const Styles = styled.div`
  padding-top: 3rem;
  margin-left: 10rem;

  i {
    font-size: 2rem;
    margin-left: 2rem;
  }

  .content {
    margin-top: 3rem;
    margin-left: 5rem;
  }

  .row {
    margin-bottom: 1rem;
  }
`;
