// import React from "react";

// export default function Requests() {
//   return (
//     <div>
//       <h1>Acc Req cccccccccccccccccccccccccccccccccccccc</h1>
//     </div>
//   );
// }

import React, { useContext, useState, useEffect } from "react";
import { AccountantContext } from "../../context/accountantContext";
import styled from "styled-components";
import { updateExpenseClaim } from "../../services/ExpenseForm";
import { updateMedicalClaim } from "../../services/MedicalForm";
import Confirmpopup from "./Confirmpopup";
import ReqTable from "./ReqTable";

const Styles = styled.div`
  padding-top: 3rem;
  margin-left: 20rem;

  i {
    margin: 4px;
  }
`;

const Requests = () => {
  const {
    requests,
    setRequests,
    medicalRequests,
    setMedicalRequests,
  } = useContext(AccountantContext);

  requests.sort((a, b) => {
    return a.expenseFormId - b.expenseFormId;
  });

  medicalRequests.sort((a, b) => {
    return a.medicalFormId - b.medicalFormId;
  });

  const [openPopup, setOpenPopup] = useState(false);
  const [idtoupd, setIdtoupd] = useState(null);
  const [act, setAct] = useState(null);
  const [decision, setDecision] = useState(false);
  const [claimType, setClaimType] = useState("");

  const changeStatus = (requestNo, func, type) => {
    setOpenPopup(true);
    setIdtoupd(requestNo);
    setAct(func);
    setClaimType(type);
  };

  useEffect(() => {
    if (decision && claimType === "expense") {
      let changedRequest = requests.find(
        (request) => request.expenseFormId === idtoupd
      );

      if (act === 1) {
        changedRequest.progressStatus = "accepted by accountant";
      } else {
        changedRequest.progressStatus = "rejected by accountant";
      }

      updateExpenseClaim(changedRequest)
        .then((res) => {
          const filteredItems = requests.filter(
            (request) => request.expenseFormId !== idtoupd
          );
          const newItems = [...filteredItems, res];
          setRequests(newItems);
          setIdtoupd(null);
          setAct(null);
          setDecision(false);
        })
        .catch((err) => {
          console.log(err);
        });
    } else if (decision && claimType === "medical") {
      let changedRequest = medicalRequests.find(
        (request) => request.medicalFormId === idtoupd
      );

      if (act === 1) {
        changedRequest.progressStatus = "accepted by accountant";
      } else {
        changedRequest.progressStatus = "rejected by accountant";
      }

      updateMedicalClaim(changedRequest)
        .then((res) => {
          const filteredItems = medicalRequests.filter(
            (request) => request.medicalFormId !== idtoupd
          );
          const newItems = [...filteredItems, res];
          setMedicalRequests(newItems);
          setIdtoupd(null);
          setAct(null);
          setDecision(false);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, [decision]);

  return (
    <Styles>
      <div>
        <h3>Expense Claim</h3>
      </div>

      <ReqTable
        requests={requests}
        url="/accountant/requests"
        type="expense"
        changeStatus={changeStatus}
      />

      <div>
        <h3>Medical Claim</h3>
      </div>

      <ReqTable
        requests={medicalRequests}
        url="/accountant/requests"
        type="medical"
        changeStatus={changeStatus}
      />

      <Confirmpopup
        openPopup={openPopup}
        setOpenPopup={setOpenPopup}
        act={act}
        setAct={setAct}
        decision={decision}
        setDecision={setDecision}
      ></Confirmpopup>
    </Styles>
  );
};

export default Requests;
