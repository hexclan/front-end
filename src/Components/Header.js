import React, { useState, useContext } from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import { makeStyles } from "@material-ui/styles";
import logo2 from "../assets/logo2.svg";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import NotificationsIcon from "@material-ui/icons/Notifications";
import VisibilityIcon from "@material-ui/icons/Visibility";
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import MenuItem from "@material-ui/core/MenuItem";

import { Link } from "react-router-dom";



import { useTheme } from "@material-ui/core/styles";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import MenuIcon from "@material-ui/icons/Menu";
import MenuList from "@material-ui/core/MenuList";

import { ContextHead } from "../context/ContextHead";
import { Menu } from "@material-ui/core";
import { GlobalContext } from "../context/GlobalContext";

function ElevationScroll(props) {
  const { children } = props;

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

const useStyles = makeStyles((theme) => ({
  toolbarMargin: {
    ...theme.mixins.toolbar,
  },

  appbar: {
    background: "#ec2329",
    zIndex: theme.zIndex.modal + 1,
  },
  logo: {
    height: "3em",
  },

  avatar: {
    width: theme.spacing(4),
    height: theme.spacing(4),
    background: theme.palette.background.paper,
    color: "black",
  },

  grow: {
    flexGrow: 1,
  },

  root: {
    display: "flex",
  },

  menu: {
    marginLeft: "auto",
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
  typo: {
    fontFamily: "Raleway",
    fontWeight: 700,
    textTransform: "capitalize",
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  listtext: {
    color: "black",
  },

  menu1: {
    color: "black",

    "&:hover": {
      Color: "white",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  menuitem: {
    ...theme.typography.tab,

    "&:hover": {
      backgroundColor: "#EC2329",
      color: "white",
    },
  },
  menuButton: {
    marginRight: theme.spacing(2),

    [theme.breakpoints.up("md")]: {
      display: "none",
    },
    "&:hover": {
      backgroundColor: "transparent",
    },
  },
}));

export default function Header() {
  const {  logOut, dispatch } = useContext(GlobalContext);
const userDetails=JSON.parse(localStorage.getItem('user'))
  const onLogOut = (e) => {
    e.preventDefault();
   
    dispatch(logOut());
    localStorage.removeItem("token");
    localStorage.removeItem("user");
  };
  const classes = useStyles();

  const [anchorEl, SetAnchorEl] = React.useState(null);
  const [anchorElSub, SetAnchorElSub] = React.useState(null);

  const theme = useTheme();

  const { mobileOpen, userPermission } = React.useContext(ContextHead);
  const [mobileOpen1, setMobileOpen1] = mobileOpen;
  const [userPermission1, setuserPermission1] = userPermission;


  const role = userDetails.employee.userRole[0].roleName;

  const handleClick = (e) => {
    SetAnchorEl(e.currentTarget);
  };
  const handleClickSub = (e) => {
    SetAnchorElSub(e.currentTarget);
  };

  const handleClose = (e) => {
    SetAnchorEl(null);

    SetAnchorElSub(null);
  };

 

  return (
    <div className={classes.root}>
      <ElevationScroll>
        <AppBar position="fixed" className={classes.appbar} color="primary">
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={() => setMobileOpen1(!mobileOpen1)}
              className={classes.menuButton}
              style={{ outline: "none" }}
            >
              <MenuIcon />
            </IconButton>

            <img className={classes.logo} alt="companylogo" src={logo2} />

            <div className={classes.grow} />

            <IconButton color="inherit" style={{ outline: "none" }}>
              <NotificationsIcon />
            </IconButton>

            <IconButton
              color="inherit"
              aria-controls="simple-menu"
              aria-haspopup="true"
              onClick={(event) => handleClick(event)}
              style={{ outline: "none" }}
            >
              <Typography className={classes.typo}>
                {userDetails.employee.firstName}
                <span>&nbsp; &nbsp;</span>{" "}
              </Typography>
              <Avatar className={classes.avatar} src="/broken-image.jpg" />
              <ArrowDropDownIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
      </ElevationScroll>

      <Menu
        open={Boolean(anchorEl)}
        anchorEl={anchorEl}
        id="simple-menu"
        classes={{ paper: classes.menu1 }}
        onClose={handleClose}
        getContentAnchorEl={null}
        anchorOrigin={{ vertical: "bottom", horizontal: "center" }}
      >
        <MenuList style={{ outline: "none" }}>
          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            component={Link}
            to="/employee"
          >
            <SupervisorAccountIcon /> <span>&nbsp; &nbsp;</span> Profile{" "}
          </MenuItem>

          <MenuItem
            aria-controls="simple"
            aria-haspopup="true"
            onClick={handleClickSub}
            classes={{ root: classes.menuitem }}
          >
            {" "}
            <VisibilityIcon /> <span>&nbsp; &nbsp;</span> view As{" "}
          </MenuItem>

          <MenuItem classes={{ root: classes.menuitem }} onClick={handleClose}>
            {" "}
            <HelpOutlineIcon /> <span>&nbsp; &nbsp;</span> Help{" "}
          </MenuItem>

          <MenuItem classes={{ root: classes.menuitem }} onClick={onLogOut}>
            {" "}
            <ExitToAppIcon /> <span>&nbsp; &nbsp;</span> Logout{" "}
          </MenuItem>
        </MenuList>
      </Menu>

      <Menu
        anchorEl={anchorElSub}
        classes={{ paper: classes.menu1 }}
        id="simple"
        open={Boolean(anchorElSub)}
        getContentAnchorEl={null}
        anchorOrigin={{
          vertical: "top",
          horizontal: "left",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        onClick={handleClose}
      >
        <MenuList style={{ outline: "none" }}>
          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => setuserPermission1("emp")}
            component={Link}
            to="/select"
          >
            {" "}
            Employee{" "}
          </MenuItem>

          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => setuserPermission1("tech")}
            component={Link}
            to="/techlead/requests"
            hidden={
              role == "Accountant" || role == "Admin" || role == "Employee"
            }
          >
            {" "}
            TechLead{" "}
          </MenuItem>

          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => setuserPermission1("acc")}
            component={Link}
            to="/accountant/requests"
            hidden={role == "Techlead" || role == "Admin" || role == "Employee"}
          >
            {" "}
            Accountant{" "}
          </MenuItem>

          <MenuItem
            classes={{ root: classes.menuitem }}
            onClick={handleClose}
            onClick={() => setuserPermission1("admin")}
            component={Link}
            to="/admin"
            hidden={
              role == "Accountant" || role == "Techlead" || role == "Employee"
            }
          >
            {" "}
            Admin{" "}
          </MenuItem>
        </MenuList>
      </Menu>

      <div className={classes.toolbarMargin} />
    </div>
  );
}
