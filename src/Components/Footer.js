import React from "react"
import {makeStyles} from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid'
import twitter from '../assets/twitter.svg'
import instagram from '../assets/instagram.svg'
import facebook from '../assets/facebook.svg'

const useStyles = makeStyles(theme => ({
    toolbarMargin: {
        ...theme.mixins.toolbar
      },
    footer :{
        
        background: "#ec2329",
        width:"100%",
        height: 70,
        zIndex:1302,
        position:'relative',
        marginTop:"0rem",
        
       
    },
    maincontainer:{
        position:"absolute",
    },
    link:{
        marginLeft:20,
        color:"white",
        fontFamily:"Arial",
        fontSize:"0.75rem",
        fontWeight:"bold",
        marginTop:25


    },
    icon :{
        height:"2rem",
        width:"2rem"
    },
    socialContainer:{
        position:"absolute",
        marginTop:"0em",
        right:"3em",
        [theme.breakpoints.down('xs')]: {
            right:"1rem",
          },
    }
}))

export default function Header() {
    const classes = useStyles()

return <footer className={classes.footer}>
  
    <Grid container  className={classes.maincontainer}>
        
     <Grid item className={classes.link}   xs={6} sm={3} > 
        copyright &copy; Creative softwares 2020. all rights
                    reserved
        </Grid>
    </Grid>


    <Grid container justify="flex-end" className={classes.socialContainer}  spacing={4} >
<Grid item component={"a"} href="https://www.facebook.com" rel="noopener noreferrer"
target="_blank" >
    
    <img alt="fb logo"  src={facebook} className={classes.icon}/>
    
    
</Grid>
<Grid item component={"a"} href="https://www.twitter.com" rel="noopener noreferrer"
target="_blank" >
    <img alt="twitter logo"  src={twitter} className={classes.icon}/>
</Grid>
<Grid item component={"a"} href="https://www.instagram.com" rel="noopener noreferrer"
target="_blank">
    <img alt="instagram logo"  src={instagram} className={classes.icon}/>
</Grid>
    </Grid>
    </footer>
}