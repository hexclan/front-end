import React, { createContext, useState, useEffect } from "react";
import { getAllExpenseClaims } from "../services/ExpenseForm";
import { getAllMedicalClaims } from "../services/MedicalForm";

export const AccountantContext = createContext();

export const AccountantProvider = (props) => {
  const [requests, setRequests] = useState([]);
  const [medicalRequests, setMedicalRequests] = useState([]);

  useEffect(async () => {
    getAllMedicalClaims()
      .then((res) => {
        setMedicalRequests(res);
      })
      .catch((err) => {
        console.log(err);
      });

    getAllExpenseClaims()
      .then((res) => {
        setRequests(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  console.log(requests);
  console.log(medicalRequests);
  return (
    <AccountantContext.Provider
      value={{ requests, setRequests, medicalRequests, setMedicalRequests }}
    >
      {props.children}
    </AccountantContext.Provider>
  );
};
