import React, { createContext, useState } from "react";

export const ContextHead = createContext();

export const MovieProvider = (props) => {
  const [mobileOpen, setMobileOpen] = useState(false);

  const [userPermission, setuserPermission] = useState("acc");
  //tech
  //admin
  //acc

  return (
    <ContextHead.Provider
      value={{
        mobileOpen: [mobileOpen, setMobileOpen],
        userPermission: [userPermission, setuserPermission],
      }}
    >
      {props.children}
    </ContextHead.Provider>
  );
};
