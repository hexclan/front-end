import React, { createContext, useState, useEffect } from "react";
import {
  getUsers,
  getDesignations,
  getDepartments,
  getRoles,
} from "../services/user";

export const AdminContext = createContext();

export const AdminProvider = (props) => {
  const [employees, setEmployees] = useState([]);
  const [designations, setDesignations] = useState([]);
  const [departments, setDepartments] = useState([]);
  const [roles, setRoles] = useState([]);

  useEffect(async () => {
    getUsers()
      .then((res) => {
        setEmployees(res);
      })
      .catch((err) => {
        console.log(err);
      });
    getDesignations()
      .then((res) => {
        setDesignations(res);
      })
      .catch((err) => {
        console.log(err);
      });

    getDepartments()
      .then((res) => {
        setDepartments(res);
      })
      .catch((err) => {
        console.log(err);
      });

    getRoles()
      .then((res) => {
        setRoles(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <AdminContext.Provider
      value={{
        employees,
        setEmployees,
        departments,
        setDepartments,
        designations,
        setDesignations,
        roles,
        setRoles,
      }}
    >
      {props.children}
    </AdminContext.Provider>
  );
};
