
import React,{createContext, useState,useEffect} from 'react'
import { getEmployee } from "../services/employee";


export const EmpContext = createContext();

export const EmpProvider= props =>{
     const[Emp,SetEmp]= useState([
       
       {
         userId: "jd890",
          userName :"laroshan",
          jobTitleName: "WebDeveloper and Marana Coder",
          firstName: "Romin",
          lastName: "Irani",
          preferredFullName: "Romin Irani",
          employeeCode: "E1",
          region: "CA",
          phoneNumber: "408-1234567",
          emailAddress: "romin.k.irani@gmail.com",
        }

    ]) ;
 /*useEffect(async () => {
    getEmployee(2)
      .then((res) => {
        console.log("response");
        console.log(res);
        SetEmp(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);*/
    return(
       <EmpContext.Provider value={[Emp,SetEmp]}>
           {props.children}
       </EmpContext.Provider>
    )
      
       

}