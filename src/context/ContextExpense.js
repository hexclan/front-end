
import React,{createContext, useState} from 'react'


export const ExpenseContext = createContext();

export const ExpenseProvider= props =>{
     const[Expense,SetExpense]= useState([
       
        {
            id: 1,
            report: "Expense 1",
            date: "25 February 2020",
            techlead: "Mr.Nuwan",
            amount: 3000.0,
            status: {
              message: "Awaiting Bank Processing",
              level: 2,
            },
          },
          {
            id: 2,
            report: "Expense 2",
            date: "31 February 2020",
            techlead: "Mr.NuwaElita",
            amount: 3400.0,
            status: {
              message: "Awaiting Bank Processing",
              level: 2,
            },
          },
          {
            id: 3,
            report: "Expense 3",
            date: "2 February 2020",
            techlead: "Mr.Nuwan",
            amount: 6000.0,
            status: {
              message: "Success",
              level: 3,
            },
          },

    ]) ;

    return(
       <ExpenseContext.Provider value={[Expense,SetExpense]}>
           {props.children}
       </ExpenseContext.Provider>
    )
      
       

}