import React, { createContext, useState, useEffect } from "react";
import { getExpenseClaims } from "../services/ExpenseForm";
import { getMedicalClaims } from "../services/MedicalForm";

export const RequestContext = createContext();

export const RequestProvider = (props) => {
  const [requests, setRequests] = useState([]);
  const [medicalRequests, setMedicalRequests] = useState([]);

  useEffect(async () => {
    getMedicalClaims("1003")
      .then((res) => {
        setMedicalRequests(res);
      })
      .catch((err) => {
        console.log(err);
      });

    getExpenseClaims("1003")
      .then((res) => {
        setRequests(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <RequestContext.Provider
      value={{ requests, setRequests, medicalRequests, setMedicalRequests }}
    >
      {props.children}
    </RequestContext.Provider>
  );
};
