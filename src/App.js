import React, { Component } from "react";
import "./App.css";

import { ThemeProvider } from "@material-ui/styles";
import Select from "./Pages/SelectPage";
import theme from "./Components/Theme";

import { BrowserRouter, Route } from "react-router-dom";
import ExpenseForm from "./Pages/ExpFormPage";

//techlead
import { RequestProvider } from "./context/techLeadContext";
import Requests from "./Pages/TechRequestPage";
import Reports from "./Pages/TechReportPage";
import RequestDetails from "./Components/TechLead/RequestDetails";
import ReportsDetails from "./Components/TechLead/ReportsDetails";

import Switch from "react-bootstrap/esm/Switch";
import MedicalForm from "./Pages/MedicalFormPage";

import empdetails from "./Pages/EmpDetail";
import editProfile from "./Components/Employee/EditProfile";
import report from "./Pages/EmpReportpage";

import { AdminProvider } from "./context/adminContext";
import AddUser from "./Components/Admin/AddUser";
import Admin from "./Components/Admin/Admin";
import EditUser from "./Components/Admin/EditUser";
import ViewUser from "./Components/Admin/ViewUser";
import { AccountantProvider } from "./context/accountantContext";
import AccountantRequests from "./Components/Accountant/Requests";
import AccountantReports from "./Components/Accountant/Reports";
import AccountantRequestDetails from "./Components/Accountant/RequestDetails";
import AccountantReportsDetails from "./Components/Accountant/ReportsDetails";

import progress from "./Pages/progresspage";
import AddDependent from "./Components/Admin/AddDependent";
import login from "./Pages/LoginPage.js";

import { PrivateRoute } from "./services/PrivateRoute";
import { GlobalContext } from "./context/GlobalContext";

export default class App extends Component {
  static contextType = GlobalContext;

  componentDidMount() {
    //  const loggedInUser = this.context.loggedInUser;
    const LoggedInUser = this.context.LoggedInUser;
    // console.log(isUserLoggedIn);
  }

  constructor(props) {
    super(props);

    this.state = {
      login: "true",
    };
  }

  render() {
    return (
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Switch>
            <div>
              {/* {LoggedInUser && (
             
                  <MovieProvider>
                    <Header />
                    <SideBar />
                  </MovieProvider>
             
              )} */}

              <Route exact path="/" component={login} />
              <PrivateRoute exact path="/select" component={Select} />
              <PrivateRoute exact path="/progress" component={progress} />
              <PrivateRoute exact path="/expense" component={ExpenseForm} />
              <PrivateRoute exact path="/medical" component={MedicalForm} />
              <PrivateRoute exact path="/employee" component={empdetails} />
              <PrivateRoute
                exact
                path="/employee/edit/:id"
                component={editProfile}
              />
              <PrivateRoute exact path="/report" component={report} />
              <RequestProvider>
                <PrivateRoute
                  exact
                  path="/techlead/requests"
                  component={Requests}
                />
                <PrivateRoute
                  exact
                  path="/techlead/reports"
                  component={Reports}
                />
                <PrivateRoute
                  exact
                  path="/techlead/requests/:id"
                  component={RequestDetails}
                />
                <PrivateRoute
                  exact
                  path="/techlead/reports/:id"
                  component={ReportsDetails}
                />

                {/* <PrivateRoute
                  exact
                  path="/accountant/requests"
                  component={AccRequests}
                />
                <PrivateRoute
                  exact
                  path="/accountant/reports"
                  component={AccReports}
                />
                <PrivateRoute
                  exact
                  path="/accountant/requests/:id"
                  component={AccRequestDetails}
                />
                <PrivateRoute
                  exact
                  path="/accountant/reports/:id"
                  component={AccRequestDetails}
                /> */}
              </RequestProvider>
              <AccountantProvider>
                <PrivateRoute
                  exact
                  path="/accountant/requests"
                  component={AccountantRequests}
                />
                <PrivateRoute
                  exact
                  path="/accountant/reports"
                  component={AccountantReports}
                />
                <PrivateRoute
                  exact
                  path="/accountant/requests/:id"
                  component={AccountantRequestDetails}
                />
                <PrivateRoute
                  exact
                  path="/accountant/reports/:id"
                  component={AccountantReportsDetails}
                />
              </AccountantProvider>
              <AdminProvider>
                <PrivateRoute exact path="/admin" component={Admin} />

                <PrivateRoute
                  exact
                  path="/admin/add-user"
                  component={AddUser}
                />
                <PrivateRoute
                  exact
                  path="/admin/edit-user/:id"
                  component={EditUser}
                />
                <PrivateRoute
                  exact
                  path="/admin/view-user/:id"
                  component={ViewUser}
                />
                <PrivateRoute
                  exact
                  path="/admin/add-dependent/:id"
                  component={AddDependent}
                />
              </AdminProvider>
            </div>
          </Switch>
        </BrowserRouter>
      </ThemeProvider>
    );
  }
}
