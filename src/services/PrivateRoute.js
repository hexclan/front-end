import React, { useContext } from "react";
import { Redirect, Route } from "react-router-dom";
import { GlobalContext, GlobalProvider } from "../context/GlobalContext";
import SideBar from "../Components/SideBar";
import { MovieProvider } from "../context/ContextHead";
import Header from "../Components/Header";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  const { loggedInUser ,userDetails} = useContext(GlobalContext);
  return (
    <Route
      {...rest}
      render={(props) =>
        localStorage.getItem('token')? (
          <>
              <MovieProvider>
                <Header />
                <SideBar />
              </MovieProvider>
              
          <Component {...props} />
          
          </>
        ) : (
          
          <Redirect
            to={{
              pathname: "/",
              state: { from: props.location },
            }}
          />
        )
      }
    />
  );
};
