const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};

export const addFormE = async (EForm) => {
  const url = "http://localhost:8080/addExpenseClaim";
  const Eforms = await request(url, "POST", EForm);
  return Eforms;
};
export const addEmail = async (Email) => {
  const url = "http://localhost:8080/email";
  const Emails = await request(url, "POST", Email);
  return Emails;
};


export const getExpenseClaims = async (tid) => {
  const url = `http://localhost:8080/ExpenseClaimFortechlead/${tid}`;

  const expenseClaims = await request(url, "GET");

  return expenseClaims;
};

export const getAllExpenseClaims = async () => {
  const url = "http://localhost:8080/allExpenseClaims";

  const expenseClaims = await request(url, "GET");

  return expenseClaims;
};

export const updateExpenseClaim = async (exclaim) => {
  const url = `http://localhost:8080/updateExpenseClaim/${exclaim.expenseFormId}`;

  const expenseClaim = await request(url, "PUT", exclaim);

  return expenseClaim;
};
