const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};

export const getUsers = async () => {
  const url = "http://localhost:8080/employee";

  const users = await request(url, "GET");

  return users;
};

export const updateEmployee = async (user) => {
  const url = `http://localhost:8080/updateEmployee/${user.employeeId}`;

  const users = await request(url, "PUT", user);

  return users;
};
export const deleteEmployee = async (id) => {
  const url = "http://localhost:8080/deleteEmployee/" + id;

  const users = await request(url, "DELETE");

  return users;
};

export const getDesignations = async () => {
  const url = "http://localhost:8080/allDesignation";

  const designations = await request(url, "GET");

  return designations;
};

export const getDepartments = async () => {
  const url = "http://localhost:8080/allDepartment";

  const departments = await request(url, "GET");

  return departments;
};

export const getRoles = async () => {
  const url = "http://localhost:8080/allRoles";

  const roles = await request(url, "GET");

  return roles;
};

export const addUser = async (user) => {
  const url = "http://localhost:8080/addemployee";
  const users = await request(url, "POST", user);
  return users;
};
export const addDependent = async (dependent) => {
  const url = "http://localhost:8080/addDependent";
  const dependents = await request(url, "POST", dependent);
  return dependents;
};

export const getDependentForEmployee = async (eid) => {
  const url = `http://localhost:8080/dependentForEmployee/${eid}`;

  const designations = await request(url, "GET");

  return designations;
};
export const editProfile = async (currentProfile) => {
  const url = `http://localhost:8080/updateEmployeeProfile/${currentProfile.employeeId}`;

  const editedProfile = await request(url, "PUT",currentProfile);

  return editedProfile;
};

