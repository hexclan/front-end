const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};




 
  export const addFormM = async (MForm) => {
    const url = 'http://localhost:8080/addMedicalClaim';
    const Mforms = await request(url, "POST", MForm);
    return Mforms;
  };

  export const getTech = async () => {
    const url = "http://localhost:8080/employeeTechLeads";
  
    const user = await request(url,"GET");
  
    return user;
  }

  export const getDependent = async (id) => {
    const url = `http://localhost:8080/dependentForEmployee/${id}`;
  
    const user = await request(url,"GET");
  
    return user;
  }

  export const addEmail = async (Email) => {
  const url = "http://localhost:8080/email";
  const Emails = await request(url, "POST", Email);
  return Emails;
};  

export const getMedicalClaims = async (tid) => {
  const url = `http://localhost:8080/MedicalClaimFortechlead/${tid}`;

  const medicalClaims = await request(url, "GET");

  return medicalClaims;
};

export const getAllMedicalClaims = async () => {
  const url = "http://localhost:8080/allMedicalClaims";

  const medicalClaims = await request(url, "GET");

  return medicalClaims;
};

export const updateMedicalClaim = async (medclaim) => {
  const url = `http://localhost:8080/updateMedicalClaim/${medclaim.medicalFormId}`;

  const medicalClaim = await request(url, "PUT", medclaim);

  return medicalClaim;
};
