export const SIGNIN_USER = "SIGNIN_USER";
export const LOGOUT_USER = "LOGOUT_USER";
export const SIGNUP_USER = "SIGNUP_USER";
export const SET_USERS = "SET_USERS";
export const REMOVE_USER = "REMOVE_USER";
export const EDIT_USER = "EDIT_USER";
