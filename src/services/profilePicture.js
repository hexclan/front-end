const request = async (url, method, body) => {
  const token = localStorage.getItem("token");

  const postOptions = {
    method: method,
    headers: {
      Authorization: `Bearer ${token}`,
      Accept: "application/json, text/plain, */*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  };

  const result = await fetch(url, postOptions).then((result) => {
    if (result.ok) {
      if (method !== "DELETE") {
        return result.json();
      }
      return result;
    }
    throw result.status;
  });

  return result;
};
export const getProfileForEmployee = async (eid) => {
  const url = `http://localhost:8080/ProfilePictureOfEmployee/${eid}`;

  const profiles = await request(url, "GET");

  return profiles;
};
export const addProfile = async (profile) => {
  const url = "http://localhost:8080/addProfilePicture";
  const profiles = await request(url, "POST", profile);
  return profiles;
};
export const getProfiles = async () => {
  const url = "http://localhost:8080/allProfilePictures";

  const users = await request(url, "GET");

  return users;
};
export const updateProfile = async (prof) => {
  const url = `http://localhost:8080/updateProfilePicture/${prof.employee.employeeId}`;

  const users = await request(url, "PUT", prof);

  return users;
};
