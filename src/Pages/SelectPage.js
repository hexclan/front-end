import React from 'react'
import Header from "../Components/Header"
import SideBar from "../Components/SideBar"
import MedicalTile from "../Components/selection/MedicalTile"
import ExpenseTile from "../Components/selection/ExpenseTile"
import {MovieProvider} from '../context/ContextHead'
import Footer from "../Components/Footer"
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

export default function SelectPage() {
    return (
        <div>
              {/* <MovieProvider>
           <Header  />
            <SideBar/>
            </MovieProvider> */}

            <Box display="flex" flexDirection="row" p={1} m={1} 
                
                flexWrap="wrap"
                marginLeft="25rem" 
                marginBottom="6rem"
                marginTop="3rem"
                >
           
          <Box item  >
          <MedicalTile/>
          </Box>
                    
          <Box item  >
          <ExpenseTile/>
          </Box>
          </Box>

              
             


              <Footer/>
        </div>
    )
}
