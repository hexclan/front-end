
import React from 'react';
import Box from '@material-ui/core/Box';
import logo from "../assets/logo.svg"
import {LoginForm} from '../Components/Login/LoginForm' 
import back from "../assets/back.svg"
import { makeStyles } from "@material-ui/core/styles";
import Footer from "../Components/Footer"
import Divider from '@material-ui/core/Divider';
import Hidden from '@material-ui/core/Hidden';




const useStyles = makeStyles((theme) => ({
  logo: {
    marginTop: "8rem"
  },
  box: {
    opacity: 1
  },
  back: {
    backgroundImage: `url(${back})`,
    
    backgroundPosition: 'center',
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
      width: '100vh',
        height: '100vh',
    
  }
}));

export default function FlexDirection() {
  const classes = useStyles();
  return (
    <>
      <div style={{ width: '100%' }} className={classes.back} >

        <Box display="flex"
          flexDirection="row"
          justifyContent="center"
        >
            <Hidden only={['sm', 'xs']}>
          <Box flexGrow={0.1} className={classes.box} >
            <img src={logo} className={classes.logo} alt="logo" />

          </Box>
         

          <Box flexGrow={0.2}>
            <Divider
              orientation="vertical"
              style={{ color: "black", width: "2px", height: '60%', marginTop: "5rem" }}
            />
          </Box>
          </Hidden>

          
          <Box className={classes.box} >
            <LoginForm />
          </Box>
       </Box>
     </div>
     
      <Footer />
    </>
  );
}


















// import React from 'react'
// 
// 
// import Footer from "../Components/Footer"
// import '../Components/Login/LoginForm.css'
// import Divider from '@material-ui/core/Divider';


// export default function LoginPage() {
//     return (
//         <>
//               <div className="flex-container" >

//   <div className ="logo-container">
//     {/* <img src={logo} className="App-logo" alt="logo" /> */}
//   </div>
//   <Divider
//     orientation="vertical"
//     style={{ color: "black", width: "2px", height: '60%' }}
//   />


//   </div>

//       
//            <Footer/>
//         </>
//     )
// }
