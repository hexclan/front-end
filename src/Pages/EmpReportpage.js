import { React } from "react";
import { MovieProvider } from "../context/ContextHead";
import Footer from "../Components/Footer";
import Header from "../Components/Header";
import SideBar from "../Components/SideBar";
import Report from "../Components/Employee/Report";
import { ReportProvider } from "../context/ContextReport";

import { Paper, makeStyles, Button } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  pageContent: {
    marginLeft: "17rem",
    [theme.breakpoints.down('sm')]: {
      marginLeft:"0rem"
    },
  },
  typo: {
    marginLeft: "17rem",
    [theme.breakpoints.down('sm')]: {
      marginLeft:"0rem"
    },
  },
}));

export default function EmpReportpage() {
  const classes = useStyles();
  return (
    <div>
      {/* <MovieProvider>
           <Header  />
            <SideBar/>
            </MovieProvider> */}
      <h3 className={classes.typo}>Employee Full Report </h3>
      <br />
      <ReportProvider>
        <div className={classes.pageContent}>
          <Report />
        </div>
      </ReportProvider>
      <Footer />
    </div>
  );
}
