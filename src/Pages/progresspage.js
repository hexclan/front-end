import React from 'react'
import ExpenseProgress from "../Components/Employee/ExpenseProgress"
import MedicalProgress from "../Components/Employee/MedicalProgress"
import {MovieProvider} from '../context/ContextHead'
import Footer from "../Components/Footer"
import Header from "../Components/Header"
import SideBar from "../Components/SideBar"
import {ExpenseProvider} from '../context/ContextExpense'

import {MedicalProvider} from '../context/ContextMedical'

export default function progress() {
  return (
    <div>


      {/* <MovieProvider>
           <Header  />
            <SideBar/>
            </MovieProvider> */}
            <ExpenseProvider>
      <ExpenseProgress/>
      </ExpenseProvider>
      <br/>
      <MedicalProvider>
      <MedicalProgress/>
      </MedicalProvider>
      <Footer/>
      
    </div>
  )
}



