import React from 'react'
import AddUser from '../Components/Admin/AddUser'
import Admin from '../Components/Admin/Admin'
import EditUser from '../Components/Admin/EditUser'
import ViewUser from '../Components/Admin/ViewUser'
import {AdminProvider} from '../context/adminContext'

export default function AdminPage() {
    return (
        <div>
             <AdminProvider>
            <AddUser/>
            <Admin/>
            <EditUser/>
            <ViewUser/>
            </AdminProvider>
        </div>
    )
}
