import React from 'react'

import Footer from "../Components/Footer"

import MedicalForm from "../Components/Employee/MedicalForm/MedicalForm"
import { Paper,makeStyles, Button } from '@material-ui/core';

import {StateProvider} from "../context/contexStates"


const useStyles = makeStyles(theme => ({
    pageContent: {
        margin: theme.spacing(5),
        padding: theme.spacing(3),
        marginLeft:"17rem",
        [theme.breakpoints.down('sm')]: {
            marginLeft:"0rem"
          },
    },
    typo:{
        marginLeft:"9px"
    },
   
  
    
}))


export default function MedicalFormPage() {
    const classes = useStyles();

    return (
        <div>

        
         

            <Paper className={classes.pageContent}>
            <h2  className={classes.typo}>Medical Form</h2>
           <StateProvider>
      

            <MedicalForm/>

      
            </StateProvider>
            </Paper>
            <Footer/>

        </div>
    )
}
