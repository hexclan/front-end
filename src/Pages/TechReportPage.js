import React from "react";
import Reports from "../Components/TechLead/Reports.js";
import { MovieProvider } from "../context/ContextHead";
import Footer from "../Components/Footer";
import Header from "../Components/Header";
import SideBar from "../Components/SideBar";

export default function TechReportPage() {
  return (
    <div>
      {/* <MovieProvider>
           <Header  />
            <SideBar/>
            </MovieProvider> */}
      <Reports />
      {/* <Footer /> */}
    </div>
  );
}
