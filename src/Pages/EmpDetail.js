import React from 'react'
import {MovieProvider} from '../context/ContextHead'
import Footer from "../Components/Footer"
import Header from "../Components/Header"
import SideBar from "../Components/SideBar"
import EmpDetails from "../Components/Employee/EmpDetails"
import {EmpProvider} from '../context/ContextEmp'

export default function EmpDetail() {
    return (
        <div>

{/* 
<MovieProvider>
           <Header  />
            <SideBar/>
            </MovieProvider> */}
            <EmpProvider>
<EmpDetails/>
</EmpProvider>
            <Footer/>
            
        </div>
    )
}
